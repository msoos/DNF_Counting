/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 * Copyright (c) 2008, MayBMS Development Group
 *  
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
    */
     

#include <cassert>

#include "../include/DKL.h"

using std::cout;
using std::endl;

/* Adapted from MayBMS implementation of AA algorithm in source/src/backend/maybms/aconf.c 
 * 
 * 
 * Algorithm modified from MayBMS to use only two sets of random variables Z_i and Z_i' as defined in the paper.
 * In particular, the results S_sra and N_sra from Step 1 (Stopping Rule algorithm) are used again in Step 3.
 * Lines with the comment 'Sample Once Modification' are the ones added / changed from the original MayBMS implementation.
 */
int DKL::AA(){
	const double_t e = 2.718281828459;

   	const double_t upsilon  = 4.0 * (e - 2.0) * log(2 / delta)
                            / (epsilon * epsilon);

   	const double_t upsilon2 = 2.0 * (1.0 + sqrt(epsilon))
                         * (1.0 + 2.0 * sqrt(epsilon))
                         * (1.0 + log(1.5))/log(2/delta) * upsilon;

	double_t mu_hat;
 	double_t rho_hat;

	uint64_t N_sra = 0;                                          // SAMPLE_ONCE_MODIFICATION
	double_t S_sra = 0;											 // SAMPLE_ONCE_MODIFICATION
   	/* Step 1: stopping rule algorithm */
	{
		const double_t epsilon_sra =
			(0.5 < sqrt(epsilon)) ? 0.5 : sqrt(epsilon);
		const double_t delta_sra = delta/3;
		const double_t upsilon_sra  = 4.0 * (e - 2.0)
                       * log(2.0 / delta_sra)
                       / (epsilon_sra * epsilon_sra);
                         
		const double_t upsilon1_sra = 1.0 + (1.0 + epsilon_sra)
		                        * upsilon_sra;
		cout<<"Starting stopping rule. Upsilon = "<<upsilon1_sra<<endl;
		uint64_t N = 0;
		double_t S = 0;

		while(S < upsilon1_sra)
		{
		   S_sra = S;											 // SAMPLE_ONCE_MODIFICATION
		   N++;
		   S += generateSample();
		}
		
		mu_hat = upsilon1_sra / (double_t)N;
		N_sra = N;												 // SAMPLE_ONCE_MODIFICATION
	}
	cout<<"Finished stopping rule. MU:"<<mu_hat<<endl;
   	/* Step 2: */
	{
   		const double_t N = ceil(upsilon2 * epsilon / mu_hat);
 	  	double_t S = 0;
		uint64_t i;
		cout<<"Estimating rho. N = "<<N<<endl;
   		for(i = 1; i <= N; i++)
   		{
      		   double_t kl2 = generateSample()
		            - generateSample();
      		   S += kl2 * kl2 / 2.0;
   		}
   
 	  	rho_hat = (S / N > epsilon * mu_hat) ?
			  (S / N) : (epsilon * mu_hat);
		cout<<"Done estimatingRho. S / N = "<< (S/N)<< " ..epsilon*mu_hat = "<<epsilon*mu_hat<<" ..Rho:"<<rho_hat<<endl;
	}
	
   	/* Step 3: */
	//{
		const double_t N = upsilon2 * rho_hat / (mu_hat * mu_hat);
		cout<<"Den(N) (no. of samples to generate for final count):"<<N<<endl;
 	  	cout<<"N_sra = "<<N_sra<<" S_sra = "<<S_sra<<endl;			// SAMPLE_ONCE_MODIFICATION
 	  	if (N_sra > N){												// SAMPLE_ONCE_MODIFICATION
			count.num = S_sra;										// SAMPLE_ONCE_MODIFICATION
			count.den = N_sra;										// SAMPLE_ONCE_MODIFICATION
			return(1);												// SAMPLE_ONCE_MODIFICATION
		}
 	  	double_t S = S_sra;											// SAMPLE_ONCE_MODIFICATION
		uint64_t i = N_sra;											// SAMPLE_ONCE_MODIFICATION
		
		//double_t S = 0;
		//uint64_t i = 0;
		
   		for(; i < N; i++)
		   S += generateSample();

   		
	//}
	
	cout<<"Num:"<<S<<endl;
	count.num = S;
	count.den = N;
	
	return (1);
}
