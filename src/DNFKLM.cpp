/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
    
#include <vector>

#include "../include/DNFKLM.h"
#include "../include/Timers.h"

#include <iostream>
#include <iomanip>

using std::cout;
using std::endl;

bool DNFKLM::satisfies(vector<bool> s, uint64_t clsnum){
	for(int i=0;i<F.clauses[clsnum].size();i++){
		if(s[abs(F.clauses[clsnum][i])-1]!=(F.clauses[clsnum][i]>0)){
			/*cout<<"assignment:"<<endl;
			for (int k=0;k<s.size();k++){
				if (s[k]==0)
					cout<<-(k+1)<<" ";
				else
					cout<<k+1<<" ";
			}
			cout<<endl<<"clause:"<<endl;
			for(int k=0;k<F.clauses[clsnum].size();k++){
				cout<<F.clauses[clsnum][k]<<" ";
			}
			cout<<endl;
			*/
			return false;
		}
	}
	return true;
}

void DNFKLM::self_adjust_coverage_thm2(){
	
	uint64_t gtime = 0;
	uint64_t total = 0;
	uint64_t N_t = 0;
	uint64_t T = ceil(((8*(1+eps)*F.m*log(3/delta))/((1 - eps*eps/8)*eps*eps)));
	cout<<"T = "<<T<<endl;
	
	//trial
	while(true){
		
		//cout<<"Getting random assnmnt"<<endl;
		vector<bool> s = DS->randomAssignment();
		//cout<<"Got random assnmnt"<<endl;
		bool finish = false;
		//step:
		while(true){
			
			gtime ++;
			if(gtime > T){
				finish = true;
				break;
			}
			uint64_t j = rb.getRandInt(uid);
			if (satisfies(s, j)){
				break;
			}
		}
		if(finish){
			break;
		}
		
		total = gtime;
		N_t ++;
	}
	//handle multiplication by U separately in calling function
	cout<<" N_t = "<<N_t<<endl;
	cout<<"Num = "<<total<<" Den = "<<F.m*N_t<<endl;
	cout<<"mu = "<<(double_t)total/(F.m*N_t)<<endl;
	count.num = total;
	count.den = F.m*N_t;
}

int DNFKLM::solve(){
	double startTime = cpuTimeTotal();
	cout<<"starting counting"<<endl;
	self_adjust_coverage_thm2();
	DS->calculateFraction(count.num, count.den);
	//cout<<"finished counting"<<endl;
	double elapsedTime = cpuTimeTotal() - startTime;
	cout<<"DNFKLM took " << std::setprecision(2) <<elapsedTime<<" seconds"<<endl;
	//cout<<"DNFKLM final result "<<":"<< F.n<<":"<<F.m<<":"<<elapsedTime<<endl;
	return 0;
}
int main(int argc, char* argv[]){
	cout<< "Starting DNFKLM at ";
	print_wall_time();
	cout<<endl;
	
	if (argc != 4 && argc != 5){
		cout<<"Usage: DNFKLM <eps> <delta> <input_dnf_dimacs_file> [seed]"<<endl;
		exit(1);
	}
	double_t eps = strtod(argv[1],NULL);
	double_t delta = strtod(argv[2],NULL);
	std::string dnfFile = std::string(argv[3]);
    int seed = -1;
    if (argc > 4) {
         seed = atoi(argv[4]);
         cout << "Setting seed:" << seed << endl;
    } else {
        cout << "Setting seed randomly."<< endl;
    }
	
	cout<<"DNFKLM invoked with epsilon="<<eps<<" delta="<<delta<<" dnfFile="<<dnfFile<<endl<<endl;
	DNFParser P;
	if(!P.parse_DNF_dimacs(dnfFile)){
		cout<<"Could not parse dimacs file "<<dnfFile<<endl;
		exit(1);
	}
			
	DNFKLM D(P.getF(),eps,delta, seed);
	int retcode = D.solve();
	
	cout<<endl<<endl<< "DNFKLM ended at ";
	print_wall_time();
	cout<<endl;
	
	return retcode;
}	
