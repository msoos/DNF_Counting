/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
    
#include <limits>
#include "../include/DNFKLMCUSP.h"
#include "../include/DNFParser.h"
#include "../include/Timers.h"

using std::cout;
using std::endl;
using std::stoi;


bool DNFKLMCUSP::satisfies(vector<bool> s, uint64_t clsnum){
	for(int i=0;i<F.clauses[clsnum].size();i++){
		if(s[abs(F.clauses[clsnum][i])-1]!=(F.clauses[clsnum][i]>0)){
			/*cout<<"assignment:"<<endl;
			for (int k=0;k<s.size();k++){
				if (s[k]==0)
					cout<<-(k+1)<<" ";
				else
					cout<<k+1<<" ";
			}
			cout<<endl<<"clause:"<<endl;
			for(int k=0;k<F.clauses[clsnum].size();k++){
				cout<<F.clauses[clsnum][k]<<" ";
			}
			cout<<endl;
			*/
			return false;
		}
	}
	return true;
}

int DNFKLMCUSP::getSigma(vector<bool> hashSoln, vector<bool>& sigma){
	uint32_t clsnum = 0;
	if (uniform){   //uniform cubewidths
		for (int j=0;j<numClsBits;j++){            //numClsBits = logm in case of uniform clause width
			if(hashSoln[F.n-F.minClauseSize+j]){
				clsnum += 1<<(j);
			}
		}
		clsnum++; // clause numbering starts from 1
		//cout<<"clsnum:"<<clsnum<<" m:"<<F.m<<endl;
	}
	else{
		vector<bool> clsBits(numClsBits);
		//cout<<"In getSigma: ";
		for(int j=0;j<numClsBits;j++){
			clsBits[j] = hashSoln[F.n-F.minClauseSize+j];
			//cout<<clsBits[j];
		}
		//cout<<endl;
		clsnum = DS->getClauseFromBits(clsBits);
		//cout<<"Clsnum is "<<clsnum<<endl;
		assert(clsnum>0); //KLSampler should handle clause numbering from 1 to m and not 0 to m-1.
	}
	if (clsnum > F.m){
		return -1;
	}
	int k=0,l=0;
	sigma.assign(F.n,0);
	for (int j=1;j<=F.n;j++){
		//cout<<"J:"<<j<<" k:"<<k<<" F.clauses[clsnum][k]:"<<F.clauses[clsnum][k]<<endl;
		if ((k<F.clauses[clsnum].size()) && (j == abs(F.clauses[clsnum][k]))){
			sigma[j-1] = F.clauses[clsnum][k]>0;
			k++;
			continue;
		}
		sigma[j-1] = hashSoln[l];
		l++;
	}
	if (k!=F.clauses[clsnum].size()){
		cout<<"Entire clause "<<clsnum<<" not used while enumerating assignment. k = "<<k<<" with size "<<F.clauses[clsnum].size()<<" Exiting.."<<endl;
		for(int j = 0;j<F.clauses[clsnum].size();j++)
			cout<<F.clauses[clsnum][j]<<" "<<std::flush;
		exit(1);
	}
	if (l!=F.n-k){
		cout<<"All hash bits not read while enumerating assignment. Exiting.."<<endl;
		exit(1);
	}
	
	if(countTypeFixed){
		bool wrongAsnmnt = false;
		while(l<F.n-F.minClauseSize){
			if (hashSoln[l]!=(F.clauses[clsnum][F.n-F.minClauseSize-l]>0)){
					assert(F.clauses[clsnum].size()>F.minClauseSize);
					//cout<<"Generated assignment did not satisfy generating clause. Skipping assignment.."<<endl;
					/*cout<<"Generated hashSoln is:"<<endl;
					for (int i = 0;i<hashSoln.size();i++){
						cout<<hashSoln[i]<<" ";
					}
					cout<<endl<<"Clause is "<<endl;
					for (int i = 0;i<F.clauses[clsnum].size();i++){
						cout<<F.clauses[clsnum][i]<<" ";
					}
					cout<<endl<<"l = "<<l<<" minclssize:"<<F.minClauseSize<<" n:"<<F.n<<" logm is:"<<logm<<endl;
					//exit(1);
					star*/ //end inner comment
					wrongAsnmnt = true;
					break;
			}
			l++;
		}
		if (wrongAsnmnt){
			return 0;
		}
	}
	return clsnum;
}

double_t DNFKLMCUSP::BoundedSATCount(double_t maxSolutions, bool upperBoundCheck){
	uint64_t count = 0;
    
    vector<bool> sigma = vector<bool>(F.n);
    bool last = false;
    ((RowEchelonHash*)assumps)->simplify(maxSolutions);
    uint64_t currHashCount = ((RowEchelonHash*)assumps)->RemoveAllConstraints();
    
    while (true){
		//cout<<"Getting assignment"<<endl;
		if (last){
			/*cout<<"Got last asnmnt"<<endl;
			for(int i = 0;i<hashSoln.size();i++){
				cout<<hashSoln[i]<<" ";
			}
			*/
			break;
		};
		HashSolution* hsln = ((RowEchelonHash*)assumps)->enumerateNextGraySoln(last);
		//vector<bool> hashSoln = ((RowEchelonHash*)assumps)->enumerateNextGraySoln(last);
		//cout<<"Got has asnmnt"<<endl;
		vector<bool> hashSoln = hsln->getVector();
		delete hsln;
		int retCode = getSigma(hashSoln, sigma);
		//cout<<"RetCode:"<<retCode<<endl;
		
		//Clause number out of bounds
		if (retCode == -1){
			//break;
			continue;
		}	
		
		//sigma does not satisfy ci
		else if (retCode == 0){
			continue;
		}	
		
		//sigma satisfies ci
		//estimate coverage(sigma)
		/*while(true){
			count++;
			if (count>maxSolutions*F.m){        //not required if using DNFScalApproxMC since hashCount is always startIteration
				return (((count+0.0)/F.m)+3.0);
			}
			int cls_sample = rb.getRandInt(uid);
			if (satisfies(sigma,cls_sample)){
				cout<<"Count:"<<count<<" m*maxsols:="<<F.m*maxSolutions<<endl;
				break;
			}
			else{
				//cout<<"RetCode:"<<retCode<<" cls_sample"<<cls_sample<<endl;
				assert (retCode!=cls_sample);
			}
		}*/
		while(true){
			count++;
			int cls_sample = rb.getRandInt(uid);
			if (satisfies(sigma,cls_sample)){
				//cout<<"Count:"<<count<<" m*maxsols:="<<F.m*maxSolutions<<endl;
				break;
			}
		}
		if (count>maxSolutions*F.m){    
			return (((count+0.0)/F.m)+3.0);
		}
		if (countTypeFixed) {
			if ((upperBoundCheck == true) && (log2(((count+1.0)/F.m))+currHashCount >= F.n)){
				cout<<"UpperBound reached! Returning from BoundedSAT with "<< (((count+0.0)/F.m))<<" solutions"<<endl;
				return (((count+0.0)/F.m));
			}
		}
		else{
			if ((upperBoundCheck == true) && (log2(((count+1.0)/F.m))+currHashCount - (F.n - F.minClauseSize) >= F.n)){
				cout<<"UpperBound reached! Returning from BoundedSAT with "<< (((count+0.0)/F.m))<<" solutions"<<endl;
				return (((count+0.0)/F.m));
			}
		}
	}
	return (((count+0.0)/F.m));
}
	

int DNFKLMCUSP::solve(int seed){
	
	double startTime = cpuTimeTotal();
	
	/*cout<<"Clauses are:"<<endl;
	for( int i = 0;i<F.clauses.size();i++){
		for(int j =0; j<F.clauses[i].size();j++){
			cout<<F.clauses[i][j]<<" ";
		}
		cout<<endl;
	}*/
	//exit(1);
	
    SATCount solCount;
    cout<<"Starting search. pivot,tApproxMC,searchMode = "<<pivotApproxMC<<" "<<tApproxMC<<" "<<searchMode<<endl;
    cout << "Using start iteration " << startIteration << endl;

    bool finished = false;
    if (searchMode == 0) {
        finished = ApproxMC(solCount);
    } else if (searchMode == 1){
        finished = ScalApproxMC(solCount);
    }
    else{
		finished = ReverseApproxMC(solCount);
	}
    if (!finished) {
        cout << " (TIMED OUT)" << endl;
        return 1;
    }

    if (solCount.hashCount == 0 && solCount.cellSolCount == 0) {
        cout << "The input formula is unsatisfiable." << endl;
        return 1;
    }

    /*if (conf.verbosity) {
        solver->print_stats();
    }*/
	if (countTypeFixed){
		cout << "Number of solutions is: " << solCount.cellSolCount
			<< " x 2^" << solCount.hashCount << endl;
	}
	else{
		cout << "Number of solutions is: " << solCount.cellSolCount
			<< " x 2^" << solCount.hashCount - (F.n - F.minClauseSize) << endl;
	}
    double elapsedTime = cpuTimeTotal() - startTime;
	
	cout<<"DNFKLMCUSP took "<<elapsedTime<<" seconds "<<endl;
	cout<<"DNFKLMCUSP final result for "<<":"<< F.n<<":"<<F.m<<":"<<elapsedTime<<endl;
	return 1;
}




