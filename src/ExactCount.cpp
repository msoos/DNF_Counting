/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
    
#include <iostream>
#include <vector>
#include <string>

#include "../include/ExactCount.h"

using std::cout;
using std::endl;
using std::stoi;

int ExactCount::solve(){
	vector<bool> asmt = vector<bool>(F.n);
	prob = 0;
	count = 0;
	while(true){
		bool sat = false;
		for(int i = 1; i<=F.m; i++){
			if (satisfies(asmt, i)){
				sat = true;
				count++;
				if (mode == 1){
					double wt = weight(asmt);
					//cout<<"Wt = "<<wt<<endl;
					prob += wt;
				}
				break;
			}
		}
		if(!sat){
			fails.push_back(vector<bool>(asmt));
		}
		if(nextAsmt(asmt)){
			for (int k = 0;k<F.n;k++){
				//cout<<asmt[k]<<" ";
			}
			//cout<<endl;
			break;	
		}
		for (int k = 0;k<F.n;k++){
			//cout<<asmt[k]<<" ";
		}
		//cout<<endl;
	}
	cout<<"Count is:"<<count<<" prob is "<<prob<<endl;
	cout<<"The "<<fails.size()<<" fails are:"<<endl;
	//exit(1);
	for(int i =0;i<fails.size();i++){
		for(int j=0;j<fails[i].size();j++){
			//cout<<fails[i][j]<<" ";
		}
		//cout<<endl;
	}
	return 1;
}

bool ExactCount::satisfies(vector<bool> s, uint64_t clsnum){
	for(int i=0;i<F.clauses[clsnum].size();i++){
		if(s[abs(F.clauses[clsnum][i])-1]!=(F.clauses[clsnum][i]>0)){
			/*cout<<"assignment:"<<endl;
			for (int k=0;k<s.size();k++){
				if (s[k]==0)
					cout<<-(k+1)<<" ";
				else
					cout<<k+1<<" ";
			}
			cout<<endl<<"clause:"<<endl;
			for(int k=0;k<F.clauses[clsnum].size();k++){
				cout<<F.clauses[clsnum][k]<<" ";
			}
			cout<<endl;
			*/
			return false;
		}
	}
	return true;
}

double_t ExactCount::weight(vector<bool> asmt){
	double tot = 1;
	for(int i = 1; i<=F.n;i++){
		if(asmt[i-1]==1){
			tot *= ((weights[i]+0.0)/(1<<bitLen));
		}
		else{
			tot *= (1 - (weights[i]+0.0)/(1<<bitLen));
		}
		//cout<<"Tot:"<<tot<<endl;
	}
	return tot;
}

bool ExactCount::nextAsmt(vector<bool>& asmt){
	for ( int i = 0; i<F.n; i++){
		if (asmt[i]==0){
			asmt[i] = 1;
			return false;
		}
		else{
			asmt[i] = 0;
		}
	}
	return true;
}	

int main(int argc, char* argv[]){
	
	if (!(argc==4)){
		cout<<"Usage: ExactCount <mode> <timeout> <input_wdnf_dimacs_file>"<<endl;
		exit(1);
	}
	DNFParser P;
	if (stoi(argv[1],NULL)==1){
		cout<<"In mode 1"<<endl;
		//exit(1);
		if(!P.parse_W2U_DNF_dimacs(argv[3])){
			cout<<"Could not parse dimacs file"<<endl;
			exit(1);
		}
		cout<<"bitLen:"<<P.getBitLen()<<endl;
		//exit(1);
		ExactCount EC(P.getF(), P.getBitLen(), P.getWeights(), stoi(argv[2],NULL));
		return EC.solve();
	} else{
		if(!P.parse_DNF_dimacs(argv[3])){
			cout<<"Could not parse dimacs file"<<endl;
			exit(1);
		}
		ExactCount EC(P.getF(), stoi(argv[2],NULL));
		return EC.solve();
	}
}
