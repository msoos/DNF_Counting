/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
    
#include "RowEchelonHash.h"
#include <iostream>
#include <sstream>
#include <chrono>
#include <thread>
#include <algorithm>
#include <iterator>
//uses mzd_echelonize_m4ri to reduce XOR+cubes. Runs much slower for moderate and large benchmarks. 
using std::cout;
using std::endl;
using std::unordered_map;
bool RowEchelonHash::initHash(uint32_t numStartHashes){
	assert(numStartHashes>= startIteration && numStartHashes<=maxHashCount);
	//cout<<"Clearing all hashes.."<<endl;
	//cout<<rectHash<<endl;
	//mzd_print(rectHash);
	mzd_set_ui(rectHash, 0);
	//cout<<"Cleared rect hashes"<<endl;
	mzd_set_ui(diagHash, 0);
	//cout<<"Cleared diag hashes"<<endl;
	
	//cout<<"Cleared diagenum hashes"<<endl;
	
	mzd_set_ui(freeVarAssnmnt,0);
	mzd_set_ui(extractHash,0);
	extractRows.clear();
	constraintRows.clear();
	//cout<<"Cleared all hashes.."<<endl;
	//cout<<"getting random bits"<<endl;
	string randBits = rb.GenerateRandomBits((numVars+1) * maxHashCount);
	//cout<<"Got random bits"<<endl;
	for (int i=0;i<startIteration;i++){
		for(int j=0;j<numVars+1;j++){
			//cout<<"Wrote bit "<<i<<" "<<j<<endl;
			mzd_write_bit(rectHash,i,j,randBits[i * (numVars+1) + j]=='1');
			//mzd_write_bit(extractHash,j,i,randBits[i * (numVars+1) + j]=='1');
		}
	}
	
	cout<<endl;
	#ifdef DBUG
	for(int i = 0; i<startIteration;i++){                //UNCOMMENT WHILE TESTING
		for(int j=0;j<numVars+1;j++){
			mzd_write_bit(testHash,i,i,1);
			mzd_write_bit(testHash,i,j+maxHashCount-numVars,mzd_read_bit(rectHash,i,j));
		}
	}
	#endif
	//cout<<"Recthash writtem"<<endl;
	for (int i=0;i<numVars;i++){
		for(int j = 0;j<numVars+1;j++){
			if (i>j){
				continue;
			}
			else if (i==j){
				mzd_write_bit(diagHash,i,j,1);
			}
			else{
				mzd_write_bit(diagHash,i,j,randBits[(i+startIteration)*(numVars+1)+j]=='1');
			}
		}
	}
	prevHashCount = 0;
	currHashCount = startIteration;                 
	numFreeVars = maxHashCount - startIteration;   //Taken care of in AddHash
	subbed = false;
	flipped = false;
	if(numStartHashes>startIteration){
		AddHash(numStartHashes-startIteration);
	}
	countFlipped = false;
	inited = true; // to purge solutions for fresh iterations
	hasConstraints = false;
	//mzd_set_ui(testHash,1);
	//mzd_write_bit(testHash,maxHashCount,maxHashCount,0);
	//mzd_print(testHash);
	
	
	//mzd_print(rectHash);
	//cout<<endl;
	//mzd_print(testHash);
	mzd_write_bit(freeVarAssnmnt,0,numVars,1);
	g = Gray(numFreeVars);
	//exit(1);
	//mzd_print(rectHash);
	//mzd_print(diagHash);
	//cout<<"numVars in hash: "<<numVars<<" startIteration "<<startIteration<<" maxHashCount "<<maxHashCount<<endl;
	//exit(0);
	
}

bool RowEchelonHash::RemoveAllConstraints(){
	//cout<<"Removing constraints.."<<endl;
	constraints.clear();
	hasConstraints= false;
	numCons=0;
	//cout<<"All constraints removed"<<endl;
}

bool RowEchelonHash::AddConstraints(string con){
	std::stringstream sline(con);
	int64_t lit;
	uint16_t i =0;
	while (sline>>lit){
		//cout<<"Lit ="<<lit<<endl;
		if(lit==0){
			break;
		}
		i++;
		constraints.push_back(lit);       //variables numbered from 1. 1 needs to be subtracted from abs(lit) while inserting in hash
	}
	
	numCons += i;
	hasConstraints = true;
}

bool RowEchelonHash::AddConstraint(uint32_t row, uint32_t col){
	//cout<<"ConstraintHash just before AddConstraint.."<<endl;
	//mzd_print(constraintHash);	
	
	if (constraintRows.find(col)!=constraintRows.end()){
		vector<uint64_t> a;
		std::set_difference(constraintRows[col].begin(), constraintRows[col].end(), constraintSkipRows.begin(), constraintSkipRows.end(), std::inserter(a, a.end()));
		//cout<<"size of a while fetching:"<<a.size()<<endl<<"listing pulls in a:"<<endl;
		for(int k=0;k<a.size();k++){
			//cout<<a[k]<<" ";
			assert(mzd_read_bit(constraintHash,a[k],col)==1);
			mzd_combine_even_in_place(constraintHash,a[k],0,constraintHash,currHashCount+row,0);
		}
		//cout<<endl;
	}
	else{
		//cout<<"origCol not found "<<origCol<< " consCol: "<<consCol<<endl<<"listing pushes in a:"<<endl;
		vector<uint64_t> a;
		for(uint64_t k=0;k<currHashCount;k++){
			if(mzd_read_bit(constraintHash,k,col)==1){
				a.push_back(k);
				//cout<<k<<" ";
			}	
		}
		//cout<<endl<<"Size of a while pushing "<<a.size()<<endl;
		constraintRows[col]=a;
		vector<uint64_t> b;
		std::set_difference(a.begin(), a.end(), constraintSkipRows.begin(), constraintSkipRows.end(), std::inserter(b, b.end()));
		for(int k = 0; k<b.size();k++){
			mzd_combine_even_in_place(constraintHash,b[k],0,constraintHash,currHashCount+row,0);
		}
		if(col==0){
			if(extractRows.size()==currHashCount-startIteration){
				extractRows.push_back(a);
			}
		}
	}
}

/*bool RowEchelonHash::AddConstraint(uint32_t row, uint32_t col){
	
	for(int k=0;k<currHashCount;k++){
		if(mzd_read_bit(constraintHash,k,col)==1){
			mzd_combine_even_in_place(constraintHash,k,0,constraintHash,currHashCount+row,0);
		}	
	}
}*/

uint32_t RowEchelonHash::simplify(uint32_t maxsols){
	
	consRank = 0;
	//grayFreeVarLocs.resize(numFreeVars);
	
	//grayBoundVarLocs.clear();
	//constraintHash = mzd_init(maxHashCount+maxConsSize,maxHashCount);
	//mzd_submatrix(constraintHash,rectHash,0,currHashCount-startIteration,maxHashCount,numVars+1);
	if(hasConstraints){
		//cout<<"Has constraints!"<<endl;
		for(int i=0;i<numFreeVars;i++){
			grayFreeVarLocs[i]=-1;
			grayBoundVarLocs[i]=-1;
		}

		for(uint32_t i = 0; i<currHashCount; i++){
			for(uint32_t j = 0; j<numVars+1; j++){
				mzd_write_bit(identity,i,startIteration+j,mzd_read_bit(rectHash,i,j));
			}
		}
		mzd_t* idWindow = mzd_init_window(identity,currHashCount,0,maxHashCount+maxConsSize,maxHashCount+1);
		mzd_set_ui(idWindow,0);
		mzd_free_window(idWindow);
		//cout<<"ConstraintHash before constraints.."<<endl;
		//mzd_print(constraintHash);	
		for (int i = 0;i<numCons;i++){
			uint64_t var = abs(constraints[i])-1;
			bool val = constraints[i]>0;
			//cout<<"i:"<<i<<"var:"<<var<<"val:"<<val<<"currhashcount:"<<currHashCount<<"numFreeVars:"<<numFreeVars<<endl;
			mzd_write_bit(identity,currHashCount+i,var,1);
			mzd_write_bit(identity,currHashCount+i,maxHashCount,val);
		}
		//cout<<"ConstraintHash after constraints.."<<endl;
		//mzd_print(identity);
		consRank = mzd_echelonize_m4ri(identity,1,0);
		consRank = consRank - currHashCount;
		//cout<<"ConstraintHash after echelonize with rank = "<<consRank<<endl;
		//mzd_print(constraintHash);
		bool noSol = true;
		for(int j = 0;j<numFreeVars;j++){
			if(mzd_read_bit(identity,currHashCount+consRank-1,maxHashCount-j-1)==1){
				noSol = false;
				break;
			}
		}
		if(noSol){
			//cout<<" No solutions "<<std::flush;
			//TODO: perform cleanup
			
			//mzd_free(constraintHash);
			return -1;
		}
		if(numFreeVars-consRank>log2(maxsols)){
			//cout<<endl<<"!!!!numFreeVars:"<<numFreeVars<<" consRank:"<<consRank<<" maxsols:"<<maxsols<<"!!!"<<endl;
			//
			//mzd_free(constraintHash);
			//exit(1);
			return numFreeVars-consRank;
		}
		
		
		int freeVarPos = 0, row=0, col=0;
		while(col<numFreeVars){
			if(!mzd_read_bit(identity,row+currHashCount,maxHashCount-numFreeVars+col)){
				grayFreeVarLocs[freeVarPos]=col;
				freeVarPos++;
				col++;
			}
			else{
				grayBoundVarLocs[row]=col;
				//AddConstraint(row,col);
				row++;
				col++;
			}
		}
		mzd_submatrix(constraintHash, identity, 0, maxHashCount-numFreeVars,maxHashCount,maxHashCount+1);	
		//cout<<"ConstraintHash ready"<<endl;
	}
	else{
		mzd_submatrix(constraintHash, rectHash, 0, currHashCount-startIteration,maxHashCount,numVars+1);
	}
	//cout<<"ConstraintHash after reducing rows"<<endl;
	//mzd_print(constraintHash);
	
	mzd_t* extractWindow = mzd_init_window(extractHash,numVars-numFreeVars,0,numVars+1,maxHashCount);
	mzd_t* constraintWindow2 = mzd_init_window(constraintHash,0,0,maxHashCount,numFreeVars+1);
	//cout<<"Constraint window:"<<endl;
	//mzd_print(constraintWindow2);
	mzd_transpose(extractWindow,constraintWindow2);
	mzd_free_window(constraintWindow2);
	mzd_free_window(extractWindow);
	//mzd_free(constraintHash);
	
	//cout<<"ExtractHash with consRank = "<<consRank<<" :"<<endl;
	//mzd_print(extractHash);
	mzd_set_ui(freeVarAssnmnt,0);
	mzd_set_ui(extractAssmt,0);
	mzd_copy_row(extractAssmt,0,extractHash,numVars);
	//cout<<endl<<"ExtractAssmt: "<<endl;
	//mzd_print(extractAssmt);
	g = Gray(numFreeVars-consRank);
	return(numFreeVars - consRank);
}

bool RowEchelonHash::AddHash(uint32_t num_xor_cls){
	cout<<"addhash currHashCount:"<<currHashCount<<" num_xorcls:"<< num_xor_cls<<" startiter:"<<startIteration<<" maxhashcunt:"<<maxHashCount<<endl;
	assert(currHashCount + num_xor_cls >= startIteration && currHashCount + num_xor_cls <= maxHashCount);
	
	subbed = false;
	flipped = false;
	//cout<<"RectHash before AddHash"<<endl;
	//mzd_print(rectHash);
	for(int i=0;i<num_xor_cls;i++){
		
		mzd_copy_row(rectHash,i+currHashCount,diagHash,i+currHashCount-startIteration);
		if(extractRows.size()>currHashCount + i - startIteration){
			for(int k=0;k<extractRows[i+currHashCount-startIteration].size();k++){	
				mzd_combine_even_in_place(rectHash,extractRows[i+currHashCount-startIteration][k],0,diagHash,i+currHashCount-startIteration,0);
			}
		}
		else{
			vector<uint64_t> emp_vec = vector<uint64_t>();
			extractRows.push_back(emp_vec);
			for(int k=0;k<i+currHashCount;k++){
				if(mzd_read_bit(rectHash,k,i+currHashCount-startIteration)==1){
					extractRows[extractRows.size()-1].push_back(k);
					mzd_combine_even_in_place(rectHash,k,0,diagHash,i+currHashCount-startIteration,0);
				}	
			}
		}	
	}
	//cout<<"RectHash after AddHash"<<endl;
	//mzd_print(rectHash);
	prevHashCount = currHashCount;
	currHashCount += num_xor_cls;
	numFreeVars = numVars - ( currHashCount - startIteration );
	constraintRows.clear();
	//cout<<endl<<"ExtractHash:"<<endl;
	//mzd_print(extractHash);
	//exit(1);
	
	return true;
}

bool RowEchelonHash::AddFlippedHash(){
	cout<<"AddFlippedHash: currHashCount:"<<currHashCount<<" num_xorcls:1 startiter:"<<startIteration<<" maxhashcunt:"<<maxHashCount<<endl;
	assert(currHashCount + 1 >= startIteration && currHashCount + 1 <= maxHashCount);
	
	if(subbed){
		flipped=true;    // in revApprxMC, AddFlipped is done after subbing, so make true.
	}
	subbed=false;
	
	mzd_copy_row(rectHash,currHashCount,diagHash,currHashCount-startIteration);
	mzd_xor_bits(rectHash,currHashCount,numVars,1,1);  //complement the bit
	if(extractRows.size()>currHashCount - startIteration){
		for(int k=0;k<extractRows[currHashCount-startIteration].size();k++){	
			mzd_combine_even_in_place(rectHash,extractRows[currHashCount-startIteration][k],0,rectHash,currHashCount,0); // directly xor with complemented row. no need to flip
		}
	}
	else{
		cout<<"Warning! AddFlipped didnt have a row stored in extractRows! Should not happen!"<<endl<<"currHashCount: "<<currHashCount<<" Exiting.."<<endl;
		cout<<"length of extractRows: "<<extractRows.size()<<" startIteration : "<<startIteration<<endl;
		exit(1);
		
	}	

	//cout<<endl<<"ExtractHash:"<<endl;
	//mzd_print(extractHash);
	countFlipped = true;
	prevHashCount = currHashCount;
	currHashCount += 1;
	numFreeVars = numVars - ( currHashCount - startIteration );
	constraintRows.clear();
	return true;
}

bool RowEchelonHash::SubFlippedHash(){
	cout<<"SubFlippedHash: currHashCount:"<<currHashCount<<" num_xorcls:1 startiter:"<<startIteration<<" maxhashcunt:"<<maxHashCount<<endl;
	assert(currHashCount - 1 >= startIteration && currHashCount - 1 <= maxHashCount);
	//subbed = true;
	prevHashCount = currHashCount;
	currHashCount -= 1;
	numFreeVars = numVars - ( currHashCount - startIteration );
	
	flipped=false;
	subbed=true;
	
	for(int k=0;k<extractRows[currHashCount-startIteration].size();k++){	
		
		mzd_combine_even_in_place(rectHash,extractRows[currHashCount-startIteration][k],0,rectHash,currHashCount,0);
		
	}
	mzd_row_clear_offset(rectHash,currHashCount,0);
	constraintRows.clear();
	return true;
}

bool RowEchelonHash::SetHash(uint32_t num_xor_cls){
	cout<<"sethash startIteration:"<<startIteration<< " num_xor:"<<num_xor_cls <<" maxHash:"<<maxHashCount<<" currHashCount"<<currHashCount<<endl;
	cout<<endl;
	assert(num_xor_cls >= startIteration && num_xor_cls <= maxHashCount);
	
    if (num_xor_cls > currHashCount){
		AddHash(num_xor_cls-currHashCount);
	}
	else if (currHashCount > num_xor_cls){
		SubHash(currHashCount-num_xor_cls);
	}
	else{
		cout<<"SetHash called with equal num_xor_cls ="<<num_xor_cls<<" and currHashCount = "<<currHashCount<<endl;
		//exit(1);
	}
    
    return true;
}

bool RowEchelonHash::SubHash(uint32_t num_xor_cls){
	cout<<"SubHash: currHashCount:"<<currHashCount<<" num_xor_cls "<<num_xor_cls<<" startiter:"<<startIteration<<" maxhashcunt:"<<maxHashCount<<endl;
	assert(currHashCount - num_xor_cls >= startIteration && currHashCount - num_xor_cls <= maxHashCount);
	
	subbed = true;
	flipped = false;
	prevHashCount = currHashCount;
	currHashCount = currHashCount - num_xor_cls;
	numFreeVars = numVars - ( currHashCount - startIteration );
	//cout<<"RectHash before subhash"<<endl;
	//mzd_print(rectHash);
	for(int i=num_xor_cls-1;i>=0;i--){
		
		for(int k=0;k<extractRows[i+currHashCount-startIteration].size();k++){	
			
			mzd_combine_even_in_place(rectHash,extractRows[i+currHashCount-startIteration][k],0,rectHash,i+currHashCount,0);
			
		}
		mzd_row_clear_offset(rectHash,i+currHashCount,0);
	}
	constraintRows.clear();
	//cout<<"RectHash aftre subhash"<<endl;
	//mzd_print(rectHash);
	return true;
}


HashSolution* RowEchelonHash::enumerateNextGraySoln(bool& last){
	//cout<<"Gray enumeration!"<<endl;
	mzd_t* res = mzd_init(1,maxHashCount);
	/*cout<<"Boundvar locs: ";
	for(int i=0;i<consRank;i++){
		cout<<grayBoundVarLocs[i]<<" ";
	}
	cout<<endl<<"Freevar locs: ";
	for(int i=0;i<numFreeVars-consRank;i++){
		cout<<grayFreeVarLocs[i]<<" ";
	}*/	
	uint32_t bitPos = g.getNextPosition();
	//cout<<"Bitpos:"<<bitPos<<endl;
	mzd_copy_row(res,0,extractAssmt,0);
	
	for(int i = 0;i<consRank;i++){
		#ifdef DBUG
		assert(grayBoundVarLocs[i]!=-1);      //UNCOMMENT WHILE TESTING
		#endif
		mzd_write_bit(res,0,currHashCount+grayBoundVarLocs[i],mzd_read_bit(extractAssmt,0,i+currHashCount));
	}
	for (int i = 0;i<numFreeVars-consRank;i++){
		#ifdef DBUG
		//cout<<i<<" "<<consRank<<" "<<hasConstraints<<" "<<endl;
		assert(grayFreeVarLocs[i]!=-1);       //UNCOMMENT WHILE TESTING
		#endif
		mzd_write_bit(res,0,currHashCount+grayFreeVarLocs[i],mzd_read_bit(freeVarAssnmnt, 0 , numVars-numFreeVars+grayFreeVarLocs[i]));
	}
	
	/*cout<<endl<<"ExtractAssmt: "<<endl;
	mzd_print(extractAssmt);
	//cout<<endl<<"FreeVarasssmt:";
	//mzd_print(freeVarAssnmnt);
	//mzd_print(extractAssmt);
	cout<<"Final soln"<<endl;
	mzd_print(res);
	cout<<"ExtractHash :"<<endl;
	mzd_print(extractHash);*/
	//cout<<"Asserting testresult"<<endl;
	HashSolution* result = new HashSolution(res,mzd_hash(res));
	#ifdef DBUG
	assert(testResult(*result,true));      //UNCOMMENT WHILE TESTING
	#endif
	//cout<<"testresult asserted!"<<endl;
	if (bitPos == -1){
		//cout<<"Found last soln"<<endl;
		last = true;
	}
	else{	
		#ifdef DBUG
		assert(bitPos<numFreeVars-consRank);           //UNCOMMENT WHILE TESTING
		#endif
		mzd_xor_bits(freeVarAssnmnt,0,numVars-numFreeVars+grayFreeVarLocs[bitPos],1,1);
		mzd_combine_even_in_place(extractAssmt, 0, 0, extractHash, numVars-numFreeVars+grayFreeVarLocs[bitPos], 0); 
	}
	//cout<<"Done gray"<<endl;
	return(result);
}

bool RowEchelonHash::checkAsnmt(const HashSolution &s){
	//cout<<"Checking asnmt,,"<<endl;
	if (prevHashCount>currHashCount){
		cout<<"asnmt satisfies.. cz prevCount > currCount"<<endl;
		return true;
	}
	#ifdef DBUG
	bool actual = testResult(s,false);      //UNCOMMENT WHILE TESTING
	#endif
	mzd_t* newConstraints = mzd_init(maxHashCount,currHashCount-prevHashCount);
	mzd_t* recWind = mzd_init_window(rectHash,prevHashCount,0,currHashCount,numVars);
	mzd_t* conWind = mzd_init_window(newConstraints,maxHashCount-numVars,0,maxHashCount,currHashCount-prevHashCount);
	mzd_transpose(conWind,recWind);
	mzd_t* conWind2 = mzd_init_window(newConstraints,maxHashCount-numVars,0,maxHashCount-numFreeVars,currHashCount-prevHashCount);
	mzd_set_ui(conWind2,0);
	mzd_free_window(conWind2);
	for(int i = 0;i<currHashCount-prevHashCount;i++){
		mzd_write_bit(newConstraints,maxHashCount-numFreeVars-1-i,currHashCount-prevHashCount-1-i,1);
	}
	mzd_t* coeffs = mzd_init(1,currHashCount-prevHashCount);
	//cout<<"coeff before mul_va "<<mzd_read_bit(coeffs,0,0)<<endl;
	_mzd_mul_va(coeffs, s.getAsnmt(),newConstraints,1);
	/*cout<<"RectHash"<<endl;
	mzd_print(rectHash);
	cout<<"recWind"<<endl;
	mzd_print(recWind);
	cout<<"conWind"<<endl;
	mzd_print(conWind);
	cout<<"newConstraints"<<endl;
	mzd_print(newConstraints);
	cout<<"asnmt"<<endl;
	mzd_print(s.getAsnmt());
	cout<<"coeffs"<<endl;
	mzd_print(coeffs);
	*/
	for(int i = 0;i<currHashCount-prevHashCount;i++){
		//cout<<mzd_read_bit(coeffs,0,i)<<" "<<mzd_read_bit(rectHash,i+prevHashCount,numVars)<<endl;
		if(mzd_read_bit(coeffs,0,i)!=mzd_read_bit(rectHash,i+prevHashCount,numVars)){
			mzd_free_window(recWind);
			mzd_free_window(conWind);
			mzd_free(newConstraints);
			mzd_free(coeffs);
			//cout<<"asnmt does not satisfie"<<endl;
			#ifdef DBUG
			assert(actual==false);      //UNCOMMENT WHILE TESTING
			#endif
			return false;
		}
	}
	#ifdef DBUG
	assert(actual==true);      //UNCOMMENT WHILE TESTING
	#endif
	mzd_free_window(recWind);
	mzd_free_window(conWind);
	mzd_free(newConstraints);
	mzd_free(coeffs);
	//cout<<"asnmt satisfies"<<endl;
	return true;
}

#ifdef DBUG
bool RowEchelonHash::testResult(const HashSolution &s1, bool addCons){      //UNCOMMENT WHILE TESTING
	vector<bool> s = vector<bool>(maxHashCount);
	for(int i = 0; i<maxHashCount;i++){
		s[i] = mzd_read_bit(s1.getAsnmt(),0,i);
	}
	mzd_set_ui(testEnumHash,0);
	mzd_submatrix(testEnumHash,testHash,0,0,startIteration,maxHashCount+1);
	//cout<<"Testenum without diagHash"<<endl;
	//mzd_print(testEnumHash);
	for(int i = 0;i<currHashCount-startIteration;i++){
		for (int j = 0;j<numVars+1;j++){
			mzd_write_bit(testEnumHash,i+startIteration,j+maxHashCount-numVars,mzd_read_bit(diagHash,i,j));
		}
	}
	//cout<<"Testenum with diaghash"<<endl;
	//mzd_print(testEnumHash);
	if(countFlipped){
		mzd_write_bit(testEnumHash,currHashCount-startIteration-1+startIteration,numVars+maxHashCount-numVars,!mzd_read_bit(diagHash,currHashCount-startIteration-1, numVars));
		//cout<<"Bit was flipped in testEnumHash"<<endl;
	}
	mzd_echelonize_m4ri(testEnumHash,1,0);
	//cout<<"testEnumHash with subbed = :"<<subbed<<endl;
	//mzd_print(testEnumHash);
	//cout<<"RectHash"<<endl;
	//mzd_print(rectHash);
	for(int i =0;i<currHashCount;i++){
		for(int j=currHashCount;j<maxHashCount+1;j++){
			if(mzd_read_bit(testEnumHash,i,j)!=mzd_read_bit(rectHash,i,j-startIteration)){
				cout<<"testenumhash"<<endl;
				mzd_print(testEnumHash);
				cout<<"rectHash"<<endl;
				mzd_print(rectHash);
				cout<<"i:"<<i<<" j:"<<j<<" startItre:"<<startIteration<<endl;
				cout<<"RectHash does not match testEnumHash. exiting.."<<endl;
				exit(1);
			}
		}
	}
	//cout<<"Recthash matches testenumhash"<<endl;
	int numCons_=0;
	if(addCons){
		numCons_=numCons;
	}
	else{
		numCons_ = 0;
	}
	for (int i = 0;i<numCons_;i++){
		mzd_write_bit(testEnumHash,currHashCount+i,abs(constraints[i])-1,1);
		mzd_write_bit(testEnumHash,currHashCount+i,maxHashCount,constraints[i]>0);
	}
	for(int i= 0;i<maxHashCount;i++){
		mzd_write_bit(testEnumHash,i+currHashCount+numCons_,i,1);
		mzd_write_bit(testEnumHash,i+currHashCount+numCons_,maxHashCount,s[i]);   //maxhashcount-i-1
	}
	//cout<<"testenum after adding assignment"<<endl;
	//mzd_print(testEnumHash);
	//cout<<"Testenumhash:"<<endl;
	//mzd_print(testEnumHash);
	int testrank=mzd_echelonize_m4ri(testEnumHash,1,0);
	//mzd_print(testEnumHash);
	//cout<<"Test Vector:"<<endl;
	//for(int i =maxHashCount;i<2*maxHashCount;i++){
		//cout<<s[i-maxHashCount]<<" ";
	//}
	//exit(1);
	//cout<<endl<<"final testenumhash"<<endl;
	//mzd_print(testEnumHash);
	if(testrank!=maxHashCount){
		return false;
	}
	return true;
}
#endif
/*bool RowEchelonHash::nextFreeVarAssnmnt(){
	
	for ( int i = numVars-1; i>= numVars - numFreeVars; i--){
		if (mzd_read_bit(freeVarAssnmnt, 0, i)==0){
			mzd_write_bit(freeVarAssnmnt, 0, i, 1);
			return false;
		}
		else{
			mzd_write_bit(freeVarAssnmnt,0, i, 0);
		}
	}
	return true;
}*/

bool RowEchelonHash::nextFreeVarAssnmnt(){
	
	for ( int i = numVars-numFreeVars; i<numVars; i++){
		if (mzd_read_bit(freeVarAssnmnt, 0, i)==0){
			mzd_write_bit(freeVarAssnmnt, 0, i, 1);
			return false;
		}
		else{
			mzd_write_bit(freeVarAssnmnt,0, i, 0);
		}
	}
	return true;
}
