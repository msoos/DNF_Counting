/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
    
#include <vector>

#include "../include/WDNFDKL.h"
#include "../include/Timers.h"

using std::cout;
using std::endl;

double_t WDNFDKL::generateSample(){
	vector<bool> s = DS->randomAssignment();
	int Z1=1;
	for (int i =1;i<DS->randClsNum;i++){
		if (satisfies(s,i)){
			Z1=0;
			break;
		}
	}
	return Z1;
}

bool WDNFDKL::satisfies(vector<bool> s, uint64_t clsnum){
	for(int i=0;i<F.clauses[clsnum].size();i++){
		if(s[abs(F.clauses[clsnum][i])-1]!=(F.clauses[clsnum][i]>0)){
			/*cout<<"assignment:"<<endl;
			for (int k=0;k<s.size();k++){
				if (s[k]==0)
					cout<<-(k+1)<<" ";
				else
					cout<<k+1<<" ";
			}
			cout<<endl<<"clause:"<<endl;
			for(int k=0;k<F.clauses[clsnum].size();k++){
				cout<<F.clauses[clsnum][k]<<" ";
			}
			cout<<endl;
			*/
			return false;
		}
	}
	return true;
}

int WDNFDKL::solve(){
	uint64_t elapsedTime = cpuTimeTotal();
	cout<<"starting counting"<<endl;
	AA();
	DS->calculateFraction(count.num, count.den);
	cout<<"finished counting"<<endl;
	elapsedTime = cpuTimeTotal() - elapsedTime;
	cout<<"DNFDKL took "<<elapsedTime<<" seconds"<<endl;
	return 0;
}
int main(int argc, char* argv[]){
	
	if (!(argc==4)){
		cout<<"Usage: WDNFDKL <eps> <delta> <input_wdnf_dimacs_file>"<<endl;
		exit(1);
	}
	
	DNFParser P;
	if(!P.parse_W2U_DNF_dimacs(argv[3])){
		cout<<"Could not parse dimacs file"<<endl;
		exit(1);
	}
	double_t eps = strtod(argv[1],NULL);
	double_t delta = strtod(argv[2],NULL);
		
	WDNFDKL D(P.getF(),P.getWeights(), P.getClauseWeights(), P.getBitLen(), eps ,delta);
	return D.solve();
}	
