/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
    
#include <vector>

#include "../include/DNFDKL.h"
#include "../include/Timers.h"

using std::cout;
using std::endl;

double_t DNFDKL::generateSample(){
	vector<bool> s = DS->randomAssignment();
	uint16_t Z1=1;
	for (int i =1;i<DS->randClsNum;i++){
		if (satisfies(s,i)){
			Z1=0;
			break;
		}
	}
	return Z1;
}

bool DNFDKL::satisfies(vector<bool> s, uint64_t clsnum){
	for(int i=0;i<F.clauses[clsnum].size();i++){
		if(s[abs(F.clauses[clsnum][i])-1]!=(F.clauses[clsnum][i]>0)){
			/*cout<<"assignment:"<<endl;
			for (int k=0;k<s.size();k++){
				if (s[k]==0)
					cout<<-(k+1)<<" ";
				else
					cout<<k+1<<" ";
			}
			cout<<endl<<"clause:"<<endl;
			for(int k=0;k<F.clauses[clsnum].size();k++){
				cout<<F.clauses[clsnum][k]<<" ";
			}
			cout<<endl;
			*/
			return false;
		}
	}
	return true;
}

int DNFDKL::solve(){
	double startTime = cpuTimeTotal();
	cout<<"starting counting"<<endl;
	AA();
	DS->calculateFraction(count.num, count.den);
	cout<<"finished counting"<<endl;
	double elapsedTime = cpuTimeTotal() - startTime;
	cout<<"DNFDKL took "<<elapsedTime<<" seconds"<<endl;
	return 0;
}
int main(int argc, char* argv[]){
	cout<< "Starting DNFDKL at ";
	print_wall_time();
	cout<<endl;
	
	if (!(argc==4 || argc == 5)){
		cout<<"Usage: DNFDKL <eps> <delta> <input_dnf_dimacs_file> [seed]"<<endl;
		exit(1);
	}
	double_t eps = strtod(argv[1],NULL);
	double_t delta = strtod(argv[2],NULL);
	std::string dnfFile = std::string(argv[3]);
    int seed = -1;
    if (argc > 4) {
         seed = atoi(argv[4]);
         cout << "Setting seed:" << seed << endl;
    } else {
        cout << "Setting seed randomly."<< endl;
    }
	
	cout<<"DNFDKL invoked with epsilon="<<eps<<" delta="<<delta<<" dnfFile="<<dnfFile
	<< " seed=" << seed
	<<endl<<endl;
	DNFParser P;
	if(!P.parse_DNF_dimacs(dnfFile)){
		cout<<"Could not parse dimacs file "<<dnfFile<<endl;
		exit(1);
	}
			
	DNFDKL D(P.getF(),eps,delta, seed);
	int retcode = D.solve();
	
	cout<<endl<<endl<< "DNFDKL ended at ";
	print_wall_time();
	cout<<endl;
	
	return retcode;
}	
