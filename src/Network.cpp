/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
    
#include <iostream>
#include <utility>
#include <algorithm>
#include <vector>

#include "Network.h"

bool Network::failEdges(RandomBits rb){
	typedef boost::graph_traits<UndirectedGraph>::edge_iterator edge_iterator;
    std::pair<edge_iterator, edge_iterator> ei = boost::edges(g);
    string bits = rb.GenerateRandomBits(m);
    uint64_t i =0;
    cout<<"Failing edges.."<<endl;
    for(edge_iterator edge_iter = ei.first; edge_iter != ei.second; ++edge_iter) {
        
        if(bits[i]=='1'){
			boost::remove_edge(*edge_iter,g);
		}
		i++;
    }
    cout<<"Failed edges"<<endl;
}

bool Network::areConnected(uint64_t s, uint64_t t){
	std::vector<boost::default_color_type> color_map(boost::num_vertices(g));
	boost::queue<uint64_t> a = boost::queue<uint64_t>();
	cout<<"Queue created"<<endl;
	boost::bfs_visitor<boost::null_visitor> b = boost::bfs_visitor<boost::null_visitor>();
	cout<<"Visitor created"<<endl;
	boost::property_map<UndirectedGraph,
        boost::vertex_index_t>::type t2 = boost::get(boost::vertex_index, g);
	cout<<"property map got."<<endl;
	//boost::default_color_type t3 = color_map[0];
	//cout<<"color_map[0] = "<<t3<<endl;
	//boost::iterator_property_map<std::vector<boost::default_color_type>::iterator,boost::property_map<UndirectedGraph, boost::vertex_index_t>::type, boost::default_color_type,boost::default_color_type&>
	auto i = boost::make_iterator_property_map(color_map.begin(), t2);
	cout<<"iterator property map created"<<endl;
	boost::breadth_first_visit(g,s,a,b,i);
	//boost::breadth_first_visit(g,s,b);
	cout<<"bfv done!"<<endl;
	if (color_map[t]!=0){
		return true;
	}
	else{
		return false;
	}
}

bool Network::printGraph(){
	cout<<"printing graph... num vertices is "<<boost::num_vertices(g)<<" num edges is "<<boost::num_edges(g)<<endl;
	typedef boost::property_map<UndirectedGraph, boost::vertex_index_t>::type IndexMap;
	boost::graph_traits < UndirectedGraph >::edge_iterator ei, ei_end;
	IndexMap index = boost::get(boost::vertex_index,g);
	for (boost::tie(ei, ei_end) = edges(g); ei != ei_end; ++ei){
		cout << index[source(*ei, g)] << " -$>$ " << index[target(*ei, g)] << std::endl;
	}
	cout<<"printed graph!"<<endl;
}
