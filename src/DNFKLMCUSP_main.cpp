/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
    
#include <iostream>
#include <string>
#include "../include/Timers.h"

#include "../include/DNFParser.h"
#include "../include/DNFKLMCUSP.h"
#include "../include/tolerance_confidence_cusp.h"

using std::cout;
using std::endl;
using std::stoi;

int main(int argc, char* argv[]){
	
	cout<< "Starting DNFKLMCUSP at ";
	print_wall_time();
	cout<<endl;
	
	if (!(argc==7 || argc == 8)){
		cout<<"Usage: DNFKLMCUSP <eps> <delta> <searchMode> <timeout> <cuspLogFile> <input_dnf_dimacs_file> [seed]"<<endl;
		exit(1);
	}
	double_t eps = strtod(argv[1],NULL);
	double_t delta = strtod(argv[2],NULL);
	uint16_t searchMode = stoi(argv[3],NULL);
	uint32_t timeout = stoi(argv[4],NULL);
	std::string logFile = std::string(argv[5]);
	std::string dnfFile = std::string(argv[6]);
    int seed = -1;
    if (argc > 7) {
        seed = stoi(argv[7],NULL);
    }
	
	cout<<"DNFKLMCUSP invoked with epsilon="<<eps<<" delta="<<delta<<" searchMode="<<searchMode<<" timeout="<<timeout<<" logFile="<<logFile<<" dnfFile="<<dnfFile
	<< " seed=" << seed
	<<endl;
	
	int32_t pivot = 2*getPivot(eps); // twice because of use of stochastic cell counting. See paper.
	int32_t tApproxMC = getT(delta);

	cout<<"Using pivot = "<<pivot<<" tApproxMC = "<<tApproxMC<<endl<<endl;
	
	DNFParser P;
	if(!P.parse_DNF_dimacs(dnfFile)){
		cout<<"Could not parse dimacs file "<<dnfFile<<endl;
		exit(1);
	}
	else{
		cout<<"Successfully parsed dimacs dnfFile "<<dnfFile<<endl;
	}
	cout<<"Starting DNFKLMCUSP.."<<endl<<endl;
	
	DNFKLMCUSP DKC(P.getF(), pivot, tApproxMC, searchMode, timeout, logFile, seed);
	int retcode = DKC.solve(seed);
	
	cout<<endl<<endl<< "DNFKLMCUSP ended at ";
	print_wall_time();
	cout<<endl;
	
	return retcode;	
}
