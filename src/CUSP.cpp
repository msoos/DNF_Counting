/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 * Copyright (c) 2016, 2013, Supratik Chakraborty, Kuldeep S. Meel, Moshe Y. Vardi
 *  
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
    */
     

//#include <ctime>
#include <cstring>
//#include <errno.h>
#include <string.h>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <map>
#include <set>
#include <fstream>
//#include <sys/stat.h>
//#include <string.h>
#include <list>
#include <array>
#include <cassert>

#include "../include/Timers.h"
#include "../include/CUSP.h"

using std::cout;
using std::cerr;
using std::endl;

using std::list;
using std::map;

bool CUSP::openLogFile(std::string cuspLogFile)
{
    cusp_logf.open(cuspLogFile.c_str());
    if (!cusp_logf.is_open()) {
        cout << "Cannot open CUSP log file '" << cuspLogFile
             << "' for writing." << endl;
        exit(1);
    }
    return true;
}

bool CUSP::ReverseApproxMC(SATCount& count)
{
    count.clear();
    double_t currentNumSolutions = 0, totalNumSolutions = 0;
    vector<uint64_t> numHashList;
    vector<int64_t> numCountList;
    assumps->initHash(endIteration);  //also adds startIteration number of hashes
    
    uint64_t hashCount;
    for (uint32_t j = 0; j < tApproxMC; j++) {
        totalNumSolutions = 0;
        cout<<"ReverseApproxMC: j="<<j<<" tApproxMC = "<<tApproxMC<<endl;
        for (hashCount = endIteration; hashCount >= startIteration; hashCount--) {        //changed from hashCount = 0 to hashCount = startIteration
            
            cout << "-> Hash Count " << hashCount << endl;
            double myTime = cpuTimeTotal();
            currentNumSolutions = BoundedSATCount(pivotApproxMC + 1 - totalNumSolutions, false);
			cout<<"Currentnumsols:"<<currentNumSolutions<<" totalSOls:"<<totalNumSolutions<<endl;
            //cout << currentNumSolutions << ", " << pivotApproxMC << endl;
            cusp_logf << "ApproxMC:" << searchMode << ":"
                      << j << ":" << hashCount << ":"
                      << std::fixed << std::setprecision(2) << (cpuTimeTotal() - myTime) << ":"
                      << (int)(currentNumSolutions == (pivotApproxMC + 1)) << ":"
                      << currentNumSolutions << endl;
            if (totalNumSolutions + currentNumSolutions > pivotApproxMC + 1) {
                //less than pivotApproxMC solutions
                break;
            }
			totalNumSolutions += currentNumSolutions;
            //Found all solutions needed
            if(hashCount!=endIteration){
				assumps->SubFlippedHash();
			}
            assumps->SubHash(1);
            assumps->AddFlippedHash();
        }
        //assert(totalNumSolutions > 0);
        assumps->initHash(endIteration);
        if (totalNumSolutions==0){
		    numHashList.push_back(hashCount);
			numCountList.push_back(pivotApproxMC);
        }
        else{
			numHashList.push_back(hashCount+1);
			numCountList.push_back(totalNumSolutions);
		}
    }
    if (numHashList.size() == 0) {
        //UNSAT
        return true;
    }

    auto minHash = findMin(numHashList);
    auto hash_it = numHashList.begin();
    auto cnt_it = numCountList.begin();
    for (; hash_it != numHashList.end() && cnt_it != numCountList.end()
            ; hash_it++, cnt_it++
        ) {
        *cnt_it *= pow(2, (*hash_it) - minHash);
    }
    int medSolCount = findMedian(numCountList);

    count.cellSolCount = medSolCount;
    count.hashCount = minHash;
    return true;
}

bool CUSP::ApproxMC(SATCount& count)
{
    count.clear();
    int64_t currentNumSolutions = 0;
    vector<uint64_t> numHashList;
    vector<int64_t> numCountList;
    assumps->initHash(startIteration);   // also adds startIteration number of hashes
    cout<<"Hash initialized"<<endl;
    for (uint32_t j = 0; j < tApproxMC; j++) {
        cout<<"ApproxMC: j="<<j<<" tApproxMC = "<<tApproxMC<<endl;
        uint64_t hashCount;
        uint32_t repeatTry = 0;
        for (hashCount = startIteration; hashCount < numHashVars; hashCount++) {        //changed from hashCount = 0 to hashCount = startIteration
            cout << "-> Hash Count " << hashCount << endl;
            double myTime = cpuTimeTotal();
            currentNumSolutions = BoundedSATCount(pivotApproxMC + 1,true);

            //cout << currentNumSolutions << ", " << pivotApproxMC << endl;
            cusp_logf << "ApproxMC:" << searchMode << ":"
                      << j << ":" << hashCount << ":"
                      << std::fixed << std::setprecision(2) << (cpuTimeTotal() - myTime) << ":"
                      << (int)(currentNumSolutions == (pivotApproxMC + 1)) << ":"
                      << currentNumSolutions << endl;
            
            if (currentNumSolutions < pivotApproxMC + 1) {
                //less than pivotApproxMC solutions
                break;
            }

            //Found all solutions needed
            assumps->AddHash(1);
        }
        assumps->initHash(startIteration);
        numHashList.push_back(hashCount);
        numCountList.push_back(currentNumSolutions);
        
    }
    if (numHashList.size() == 0) {
        //UNSAT
        return true;
    }

    auto minHash = findMin(numHashList);
    auto hash_it = numHashList.begin();
    auto cnt_it = numCountList.begin();
    for (; hash_it != numHashList.end() && cnt_it != numCountList.end()
            ; hash_it++, cnt_it++
        ) {
        *cnt_it *= pow(2, (*hash_it) - minHash);
    }
    int medSolCount = findMedian(numCountList);

    count.cellSolCount = medSolCount;
    count.hashCount = minHash;
    return true;
}

bool CUSP::ScalApproxMC(SATCount& count)
{
    count.clear();
    vector<uint64_t> numHashList;
    vector<int64_t> numCountList;
    cout<<"Starting scalapproxmc.."<<endl;      //added
    assumps->initHash(startIteration);
    cout<<"hashesh initialized"<<endl;       //added
    uint64_t hashCount = startIteration, hashPrev = 0, mPrev = 0;
    double myTime = cpuTimeTotal();
    if (hashCount == 0) {
        int64_t currentNumSolutions = BoundedSATCount(pivotApproxMC+1,false);
        cusp_logf << "ApproxMC:"<< searchMode<<":"<<"0:0:"
                  << std::fixed << std::setprecision(2) << (cpuTimeTotal() - myTime) << ":"
                  << (int)(currentNumSolutions == (pivotApproxMC + 1)) << ":"
                  << currentNumSolutions << endl;

        //Din't find at least pivotApproxMC+1
        if (currentNumSolutions <= pivotApproxMC) {
            count.cellSolCount = currentNumSolutions;
            count.hashCount = 0;
            return true;
        }
        hashCount++;
    }

    for (uint32_t j = 0; j < tApproxMC; j++) {
		cout<<"ScalApproxMC: j="<<j<<" tApproxMC = "<<tApproxMC<<endl;
        map<uint64_t,int64_t> countRecord;
        map<uint64_t,uint32_t> succRecord;
        
        uint32_t repeatTry = 0;
        uint64_t numExplored = 1;
        uint64_t lowerFib = 0, upperFib = numHashVars;

        while (numExplored < numHashVars) {
            cout << "Num Explored: " << numExplored
                 << " ind set size: " << numHashVars << endl;
            myTime = cpuTimeTotal();
            uint64_t swapVar = hashCount;
            assumps->SetHash(hashCount);
            int64_t currentNumSolutions = BoundedSATCount(pivotApproxMC + 1,false);
			
            //cout << currentNumSolutions << ", " << pivotApproxMC << endl;
            cusp_logf << "ApproxMC:" << searchMode<<":"
                      << j << ":" << hashCount << ":"
                      << std::fixed << std::setprecision(2) << (cpuTimeTotal() - myTime) << ":"
                      << (int)(currentNumSolutions == (pivotApproxMC + 1)) << ":"
                      << currentNumSolutions << endl;
            
            if (currentNumSolutions < pivotApproxMC + 1) {
                numExplored = lowerFib+numHashVars-hashCount;
                if (succRecord.find(hashCount-1) != succRecord.end()
                    && succRecord[hashCount-1] == 1
                ) {
                    numHashList.push_back(hashCount);
                    numCountList.push_back(currentNumSolutions);
                    mPrev = hashCount;
                    //less than pivotApproxMC solutions
                    break;
                }
                succRecord[hashCount] = 0;
                countRecord[hashCount] = currentNumSolutions;
                if (abs(hashCount-mPrev) <= 2 && mPrev != 0) {
                    upperFib = hashCount;
                    hashCount--;
                } else {
                    if (hashPrev > hashCount) {
                        hashPrev = 0;
                    }
                    upperFib = hashCount;
                    if (hashPrev > lowerFib) {
                        lowerFib = hashPrev;
                    }
                    hashCount = (upperFib+lowerFib)/2;
                }
            } else if (currentNumSolutions >= pivotApproxMC+1) {        //changed from == to >=
                numExplored = hashCount + numHashVars-upperFib;
                if (succRecord.find(hashCount+1) != succRecord.end()
                    && succRecord[hashCount+1] == 0
                ) {
                    numHashList.push_back(hashCount+1);
                    numCountList.push_back(countRecord[hashCount+1]);
                    mPrev = hashCount+1;
                    break;
                }
                succRecord[hashCount] = 1;
                if (abs(hashCount - mPrev) < 2 && mPrev!=0) {
                    lowerFib = hashCount;
                    hashCount ++;
                } else if (lowerFib + (hashCount - lowerFib)*2 >= upperFib-1) {
                    lowerFib = hashCount;
                    hashCount = (lowerFib+upperFib)/2;
                } else {
                    //printf("hashPrev:%d hashCount:%d\n",hashPrev, hashCount);
                    hashCount = lowerFib + (hashCount -lowerFib)*2;
                }
            }
            hashPrev = swapVar;
        }
        assumps->initHash(startIteration);
        hashCount =mPrev;
    }
    if (numHashList.size() == 0) {
        //UNSAT
        return true;
    }

    auto minHash = findMin(numHashList);
    auto cnt_it = numCountList.begin();
    for (auto hash_it = numHashList.begin()
        ; hash_it != numHashList.end() && cnt_it != numCountList.end()
        ; hash_it++, cnt_it++
    ) {
        *cnt_it *= pow(2, (*hash_it) - minHash);
    }
    int medSolCount = findMedian(numCountList);

    count.cellSolCount = medSolCount;
    count.hashCount = minHash;
    return true;
}
