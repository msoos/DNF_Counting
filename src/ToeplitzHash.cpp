/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
    
#include "../include/ToeplitzHash.h"
#include <iostream>
#include <sstream>

using std::cout;
using std::endl;
bool ToeplitzHash::initHash(uint32_t numStartHashes){
	cout<<"Init called:"<< numStartHashes<<" "<<startIteration<<" "<<numVars<<endl;
	assert(numStartHashes>= startIteration && numStartHashes<=numVars);
	//cout<<"Clearing all hashes.."<<endl;
	//cout<<rectHash<<endl;
	//mzd_print(rectHash);
	mzd_set_ui(rectHash, 0);
	//cout<<"Cleared rect hashes"<<endl;
	//mzd_set_ui(diagHash, 0);
	//cout<<"Cleared diag hashes"<<endl;
	//mzd_set_ui(diagEnumHash, 0);
	//cout<<"Cleared diagenum hashes"<<endl;
	
	//cout<<"Cleared all hashes.."<<endl;
	//cout<<"getting random bits"<<endl;
	firstRow = rb.GenerateRandomBits(numVars);
	firstCol = rb.GenerateRandomBits((numVars-1));
	lastCol = rb.GenerateRandomBits(numVars);
	//cout<<"Got random bits"<<endl;
	currHashCount = 0;
	numFreeVars = numVars;
	
	AddHash(numStartHashes);
	countFlipped = false;
	//mzd_set_ui(testHash,1);
	//mzd_write_bit(testHash,maxHashCount,maxHashCount,0);
	//mzd_print(testHash);
	//cout<<endl;
	
	//mzd_print(rectHash);
	//cout<<endl;
	//mzd_print(testHash);
	
	//exit(1);
	//mzd_print(rectHash);
	//mzd_print(diagHash);
	//cout<<"numVars in hash: "<<numVars<<" startIteration "<<startIteration<<" maxHashCount "<<maxHashCount<<endl;
	//exit(0);
	
}

bool ToeplitzHash::AddConstraints(string con){
	//cout<<"Con = "<<con;
	std::stringstream sline(con);
	int64_t lit;
	uint16_t i =0;
	while (sline>>lit){
		//cout<<"Lit ="<<lit<<endl;
		if(lit==0){
			break;
		}
		i++;
		constraints.push_back(lit);  //variables numbered from 1. 1 needs to be subtracted from abs(lit) while inserting in hash
	}
	numCons = i;
	hasConstraints = true;
	//cout<<"Added Constraints"<<endl;
}

bool ToeplitzHash::RemoveAllConstraints(){
	//cout<<"Removing constraints.."<<endl;
	constraints.clear();
	hasConstraints= false;
	numCons=0;
	//cout<<"All constraints removed"<<endl;
}

uint32_t ToeplitzHash::simplify(){
	//cout<<"NumCons="<<numCons<<endl;
	if (mzd_owns_blocks(echHash)){
		//cout<<"Freeing echHash"<<endl;
		mzd_free(echHash);
		//cout<<"EchHash freed"<<endl;
	}
	if(hasConstraints){
		//cout<<"Constraints present"<<endl;
		echHash=mzd_init(numVars+numCons, numVars+1);
		//cout<<"EchHash inited"<<endl;
		mzd_submatrix(echHash, rectHash, 0, 0, currHashCount, numVars+1);
		//mzd_print(echHash);
		//cout<<"EchHash submatrixed"<<endl;
		for(int i = currHashCount; i<numCons+currHashCount; i++){
			mzd_write_bit(echHash,i,abs(constraints[i-currHashCount])-1,1);
			mzd_write_bit(echHash,i,numVars,constraints[i-currHashCount]>0);
		}
		//mzd_print(echHash);
	}
	else{
		cout<<"No constraints! Exiting.."<<endl;
		exit(1);
		echHash = mzd_init(currHashCount,numVars+1);
		mzd_submatrix(echHash, rectHash, 0, 0, currHashCount, numVars+1);
	}
	//cout<<"EchHash written"<<endl;
	echRank = mzd_echelonize_m4ri(echHash,1,0);
	//cout<<"EchHash echelonized"<<endl;
	bool noSol = true;
	for(int j = 0;j<numVars;j++){
		if(mzd_read_bit(echHash,echRank-1,j)==1){
			noSol = false;
		}
	}
	if(noSol){
		cout<<" No solutions "<<std::flush;
		return -1;
	}
	//cout<<"EchRank="<<echRank<<endl;
	int prevPivotCol = -1;
	int varRow=0;
	//mzd_print(echHash);
	for(int i=0;i<numVars;i++){
		bool pivotFound=false;
		int currCol=prevPivotCol+1;
		for(;currCol<numVars;currCol++){
			//cout<<"i: "<<i<<" currCol: "<<currCol<<endl;
			if(mzd_read_bit(echHash,i,currCol)==1){
				prevPivotCol=currCol;
				pivotFound=true;
				break;
			}
			//mzd_print(temp);
			//cout<<"Pivot not found: "<<varRow+rank<<" "<<currCol<<endl;
			//cout<<"varRow: "<<varRow<<" rank: "<<rank<<endl;
			mzd_write_bit(echHash,echRank+varRow,currCol,1);
			//mzd_print(temp);
			varRow++;
		}
		if(currCol==numVars){
			break;
		}
	}
	if(varRow!=numVars-echRank){
			cout<<"Error! no of assignments done is incorrect. varRow = "<<varRow<<" n= "<<numVars<<" Rank="<<echRank<<" Exiting.. "<<endl;
			mzd_print(rectHash);
			mzd_print(echHash);
			exit(1);
	}
	
	//cout<<"Returning echrank.."<<endl;
	//mzd_print(echHash);
	return echRank;
}

bool ToeplitzHash::AddHash(uint32_t num_xor_cls){
	cout<<"In AddHash currHashCount:"<<currHashCount<<" num_xorcls:"<< num_xor_cls<<" startiter:"<<startIteration<<" maxhashcunt:"<<maxHashCount<<endl;
	assert(currHashCount + num_xor_cls >= startIteration && currHashCount + num_xor_cls <= maxHashCount);
	
	for(int i =currHashCount;i<num_xor_cls+currHashCount;i++){
		for(int j=0;j<numVars+1;j++){
			if(j==numVars){
				mzd_write_bit(rectHash,i,j,lastCol[i]=='1');
			}
			else if(i-j<=0){
				mzd_write_bit(rectHash,i,j,firstRow[j-i]=='1');
			}
			else{
				mzd_write_bit(rectHash,i,j,firstCol[i-j-1]=='1');
			}
			//cout<<"i = "<<i<<" j = "<<j<<endl;
			//mzd_print(assumps);
			//cout<<endl;
		}
	}
	//mzd_print(rectHash);
	int rank=mzd_echelonize_m4ri(rectHash,1,0);
	//mzd_print(rectHash);
	
	currHashCount += num_xor_cls;
	//assert(rank==currHashCount);
	numFreeVars = numVars - currHashCount;
	
	return true;
}

bool ToeplitzHash::AddFlippedHash(){
	cout<<"AddFlippedHash: currHashCount:"<<currHashCount<<" num_xorcls:1 startiter:"<<startIteration<<" maxhashcunt:"<<maxHashCount<<endl;
	assert(currHashCount + 1 >= startIteration && currHashCount + 1 <= maxHashCount);
	
	

	//cout<<endl<<"ExtractHash:"<<endl;
	//mzd_print(extractHash);
	AddHash(1);
	mzd_write_bit(rectHash,currHashCount-1,numVars,!mzd_read_bit(rectHash,currHashCount-1,numVars));
	countFlipped = true;

	return true;
}

bool ToeplitzHash::SubFlippedHash(){
	cout<<"SubFlippedHash: currHashCount:"<<currHashCount<<" num_xorcls:1 startiter:"<<startIteration<<" maxhashcunt:"<<maxHashCount<<endl;
	assert(currHashCount - 1 >= startIteration && currHashCount - 1 <= maxHashCount);
	
	SubHash(1);
	return true;
}

bool ToeplitzHash::SetHash(uint32_t num_xor_cls){
	printf("SetHash: startIteration:%lu num_xor:%u maxHash:%lu",startIteration,num_xor_cls,maxHashCount);
	cout<<endl;
	assert(num_xor_cls >= startIteration && num_xor_cls <= maxHashCount && printf("startIteration:%lu num_xor:%u maxHash:%lu",startIteration,num_xor_cls,maxHashCount));
	
    if (num_xor_cls < currHashCount) {
		SubHash(currHashCount-num_xor_cls);
    } 
    else if (num_xor_cls > currHashCount) {
        AddHash(num_xor_cls-currHashCount);
    }
    
    return true;
}

bool ToeplitzHash::SubHash(uint32_t num_xor_cls){
	cout<<"SubHash: currHashCount:"<<currHashCount<<" startiter:"<<startIteration<<" maxhashcunt:"<<maxHashCount<<endl;
	assert(currHashCount - num_xor_cls >= startIteration && currHashCount - num_xor_cls <= maxHashCount);
	
	/*if (!countFlipped){
		countFlipped = true;
		currHashCount ++;
	}*/
	
	for(int i =0;i<num_xor_cls;i++){
		for(int j=0;j<numVars+1;j++){
			mzd_write_bit(rectHash,currHashCount-i,j,0);
		}
	}
	//int rank = mzd_echelonize_m4ri(echHash,1,0);
	
	currHashCount = currHashCount - num_xor_cls;
	numFreeVars = numVars-currHashCount;
	
	return true;
}


/*bool RowEchelonHash::clear(bool init){
	//cout<<"Clearing all hashes.."<<endl;
	//cout<<rectHash<<endl;
	//mzd_print(rectHash);
	mzd_set_ui(rectHash, 0);
	//cout<<"Cleared rect hashes"<<endl;
	mzd_set_ui(diagHash, 0);
	//cout<<"Cleared diag hashes"<<endl;
	mzd_set_ui(diagEnumHash, 0);
	//cout<<"Cleared diagenum hashes"<<endl;
	mzd_set_ui(rectAssnmnt,0);
	mzd_set_ui(freeVarAssnmnt,0);
	if (init){
		initHash();
	}
}*/


vector<bool>  ToeplitzHash::enumerateNextSoln(bool& last){
	mzd_submatrix(enumHash,echHash,0,0,numVars,numVars+1);
	
	//mzd_print(clauseSoln);
	//cout<<"echelonizing to get solution "<<endl;
	int testRank = mzd_echelonize_m4ri(enumHash,1,0);
	if(testRank!=numVars){
		cout<<"Error! Cannot extract assignment because matrix is not full rank. Exiting.. "<<endl;
		exit(1);
	}
	vector<bool> result = vector<bool>(numVars);
	//cout<<"result i:"<<endl;
	for(int j =0;j<numVars;j++){
		result[j]=mzd_read_bit(enumHash,j,numVars);
		//cout<<result[j]<<" ";
	}
	
	last = nextFreeVarAssnmnt();
	//for (int i =0;i<result.size();i++){
		//cout<<result[i]<<" "<<endl;
	//}
	
	//if (coun == 3){
		//exit(1);
	//}
	//coun++;
	return (result);
}


bool ToeplitzHash::testResult(vector<bool> s){
	return true;
}

bool ToeplitzHash::nextFreeVarAssnmnt(){
	
	for ( int i = echRank; i<numVars; i++){
		if (mzd_read_bit(echHash, i, numVars)==0){
			mzd_write_bit(echHash, i, numVars, 1);
			return false;
		}
		else{
			mzd_write_bit(echHash,i,numVars,0);
		}
	}
	return true;
}
