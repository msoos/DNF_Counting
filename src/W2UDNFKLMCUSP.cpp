/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
    
#include <iostream>


#include "../include/W2UDNFKLMCUSP.h"
#include "../include/DNFParser.h"
#include "../include/Timers.h"
#include "../include/RandomBits.h"

using std::cout;
using std::endl;
using std::stoi;


int W2UDNFKLMCUSP::getSigma(vector<bool> hashSoln_, vector<bool>& sigma){    //clauseWeights and weights start from 1. check if accounted for.
	cout<<"hashsoln_ in getSigma:"<<endl;
	for(int i = 0;i<hashSoln_.size();i++){
		cout<<hashSoln_[i]<<" ";
	}
	cout<<endl;
	int clsnum_ = 0;
	for (int j=0;j<clsHashWidth;j++){
		if(hashSoln_[(F.n-F.minClauseSize)*bitLen+j]){
			clsnum_ += 1<<(j);
		}
	}
	cout<<"clsnum_:"<<clsnum_<<endl;
	//cout<<"clsnum:"<<clsnum<<" m:"<<F.m<<endl;
	vector<int64_t>::iterator pos = lower_bound(clsBitMap.begin(),clsBitMap.end(),clsnum_);
	int clsnum = pos - clsBitMap.begin();
	cout<<"clsnum:"<<clsnum<<endl;
	assert(clsnum>=1);
	if (clsnum > F.m){  //clsnum can be equal to F.m since clause numbering starts from 1 
		//return 0;
		return -1;
	}
	
	vector<uint32_t> hashSoln = vector<uint32_t>(F.n - F.minClauseSize);
	for ( int k = 0; k< F.n- F.minClauseSize; k++){
		int varnum = 0;       //integer value of the assignment for the variable in position k
		for( int j = 0; j<bitLen;j++){
			if(hashSoln_[k*bitLen+j]){
				varnum += 1<<(j);
			}
		}
		hashSoln[k] = varnum;
	}
	cout<<"hashsoln in getSigma:"<<endl;
	for(int i = 0;i<hashSoln.size();i++){
		cout<<hashSoln[i]<<" ";
	}
	cout<<endl;
	int k=0,l=0;
	sigma.assign(F.n,0);
	for (int j=1;j<=F.n;j++){
		//cout<<"J:"<<j<<" k:"<<k<<" F.clauses[clsnum][k]:"<<F.clauses[clsnum][k]<<endl;
		if ((k<F.clauses[clsnum].size()) && (j == abs(F.clauses[clsnum][k]))){
			sigma[j-1] = F.clauses[clsnum][k]>0;
			k++;
			continue;
		}
		sigma[j-1] = hashSoln[l]<weights[j];  //weights indexed from 1 to n
		l++;
	}
	cout<<"Sigma is :"<<endl;
	for(int i = 0;i<sigma.size();i++){
		cout<<sigma[i]<<" ";
	}
	cout<<endl;
	if (k!=F.clauses[clsnum].size()){
		cout<<"Entire clause "<<clsnum<<" not used while enumerating assignment. k = "<<k<<" with size "<<F.clauses[clsnum].size()<<" Exiting.."<<endl;
		for(int j = 0;j<F.clauses[clsnum].size();j++)
			cout<<F.clauses[clsnum][j]<<" "<<std::flush;
		exit(1);
	}
	if (l!=F.n-k){
		cout<<"All hash bits not read while enumerating assignment. Exiting.."<<endl;
		exit(1);
	}
	bool wrongAsnmnt = false;
	while(l<F.n-F.minClauseSize){
		if ( (hashSoln[l]<weights[abs(F.clauses[clsnum][F.n-F.minClauseSize-l])]) != ((F.clauses[clsnum][F.n-F.minClauseSize-l]>0)) ){
				assert(F.clauses[clsnum].size()>F.minClauseSize);
				cout<<"Generated assignment did not satisfy generating clause. Skipping assignment.."<<endl;
				/*cout<<"Generated hashSoln is:"<<endl;
				for (int i = 0;i<hashSoln.size();i++){
					cout<<hashSoln[i]<<" ";
				}
				cout<<endl<<"Clause is "<<endl;
				for (int i = 0;i<F.clauses[clsnum].size();i++){
					cout<<F.clauses[clsnum][i]<<" ";
				}
				cout<<endl<<"l = "<<l<<" minclssize:"<<F.minClauseSize<<" n:"<<F.n<<" logm is:"<<logm<<endl;
				//exit(1);
				*/
				wrongAsnmnt = true;
				break;
		}
		l++;
	}
	if (wrongAsnmnt){
		//return 0;
	}
	return clsnum;
}

/*
int64_t DNFKLMCUSP::BoundedSATCount(uint32_t maxSolutions){
	uint64_t count = 0;
    std::uniform_int_distribution<unsigned> uid {1,F.m};
    RandomBits rb;
    rb.SeedEngine();
    vector<bool> sigma = vector<bool>(F.n);
    bool last = false;
    while (true){
		//cout<<"Getting assignment"<<endl;
		vector<bool> hashSoln = ((RowEchelonHash*)assumps)->enumerateNextSoln(last);
		//cout<<"Got has asnmnt"<<endl;
		if (last){
			cout<<"Got last asnmnt"<<endl;
			for(int i = 0;i<hashSoln.size();i++){
				cout<<hashSoln[i]<<" ";
			}
			
			break;
		}
		;
		int retCode = getSigma(hashSoln, sigma);
		
		//Clause number out of bounds
		if (retCode == -1){
			return ((count+0.0)/F.m);
		}	
		
		//sigma does not satisfy ci
		else if (retCode == 0){
			continue;
		}	
		
		//sigma satisfies ci
		//estimate coverage(sigma)
		while(true){
			count++;
			if (count>maxSolutions*F.m){
				return (((count+0.0)/F.m)+3.0);
			}
			int cls_sample = rb.getRandInt(uid);
			if (satisfies(sigma,cls_sample)){
				cout<<"Count:"<<count<<" m*maxsols:="<<F.m*maxSolutions<<endl;
				break;
			}
			else{
				//cout<<"RetCode:"<<retCode<<" cls_sample"<<cls_sample<<endl;
				assert (retCode!=cls_sample);
			}
		}
			
	}
	return (((count+0.0)/F.m));
}
*/
/*
int64_t DNFKLMCUSP::BoundedSATCount(uint32_t maxSolutions)
{
    uint64_t count = 0;
    std::uniform_int_distribution<unsigned> uid {1,m};
    bool last = false;
    while (true){
		vector<bool> hashSoln = assumps.enumerateNextSoln(last);
		if (last){
			break;
		}
		int clsnum = 0;
		for (int j=logpivot;j<logpivot+logm;j++){
			if(hashSoln[n-minClauseSize+j]){
				clsnum += 1<<(j-logpivot);
			}
		}
		clsnum++; // clause numbering starts from 1
		//cout<<"clsnum:"<<clsnum<<" m:"<<m<<endl;
		if (clsnum>m){
			return ((count+0.0)/m);
		}
		int k=0,l=0;
		vector<bool> sigma = vector<bool>(n);
		for (int j=0;j<n;j++){
			if (j == abs(clauses[clsnum][k])){
				sigma[j] = clauses[clsnum][k]>0;
				k++;
				continue;
			}
			sigma[j] = hashSoln[l];
			l++;
		}
		if (k!=clauses[clsnum].size()){
			cout<<"Entire clause "<<clsnum<<" not used while enumerating assignment. k = "<<k<<" Exiting.."<<endl;
			for(int j = 0;j<clauses[clsnum].size();j++)
				cout<<clauses[clsnum][j]<<" "<<std::flush;
			exit(1);
		}
		if (l!=n-k){
			cout<<"All hash bits not read while enumerating assignment. Exiting.."<<endl;
			exit(1);
		}
		bool wrongAsnmnt = false;
		while(l<n-minClauseSize){
			if (hashSoln[l]!=(clauses[clsnum][n-minClauseSize-l]>0)){
					cout<<"Generated assignment did not satisfy generating clause. Skipping assignment.."<<endl;
					wrongAsnmnt = true;
					break;
			}
			l++;
		}
		if (wrongAsnmnt){
			continue;
		}
		while(true){
			count++;
			if (count>maxSolutions*m){
				//mzd_free(enumHash);
				return (((count+0.0)/m)+3.0);
			}
			int cls_sample = uid(randomEngine);
			if (satisfies(sigma,clauses[cls_sample])){
				cout<<"Count:"<<count<<" m*maxsols:="<<m*maxSolutions<<endl;
				break;
			}
		}
	}

	return (((count+0.0)/m));
}
*/

int W2UDNFKLMCUSP::solve(){
	
	double startTime = cpuTimeTotal();
	
	cout<<"Clauses and clause weights are:"<<endl;
	for( int i = 0;i<F.clauses.size();i++){
		for(int j =0; j<F.clauses[i].size();j++){
			cout<<F.clauses[i][j]<<" ";
		}
		cout<<"           "<<clauseWeights[i]<<endl;
	}
	//exit(1);
	
    SATCount solCount;
    cout<<"Starting search. pivot,tApproxMC,searchMode = "<<pivotApproxMC<<" "<<tApproxMC<<" "<<searchMode<<endl;
    cout << "Using start iteration " << startIteration << endl;

    bool finished = false;
    if (searchMode == 0) {
        finished = ApproxMC(solCount);
    } else if (searchMode == 1){
        finished = ScalApproxMC(solCount);
    }
    else{
		finished = ReverseApproxMC(solCount);
	}
    if (!finished) {
        cout << " (TIMED OUT)" << endl;
        return 1;
    }

    if (solCount.hashCount == 0 && solCount.cellSolCount == 0) {
        cout << "The input formula is unsatisfiable." << endl;
        return 1;
    }

    /*if (conf.verbosity) {
        solver->print_stats();
    }*/

    cout << "Number of solutions is: " << solCount.cellSolCount
         << " x 2^" << solCount.hashCount << endl;
	cout << "Probability is: "<< (pow(2,log2(solCount.cellSolCount)+solCount.hashCount-((F.n-F.minClauseSize)*bitLen+clsHashWidth))) <<endl;
    double elapsedTime = cpuTimeTotal() - startTime;
	
	cout<<"W2UDNFKLMCUSP took "<<elapsedTime<<" seconds "<<endl;
	cout<<"W2UDNFKLMCUSP final result for "<<":"<< F.n<<":"<<F.m<<":"<<elapsedTime<<endl;
	return 1;
}

void W2UDNFKLMCUSP::filterClauses(double_t filterWt){
	minClsWt = 1;
	int i = 1;
	while ( i < F.clauses.size() ) {
		if ( clauseWeights[i] < filterWt ) {
			clauseWeights.erase( clauseWeights.begin() + i );
			F.clauses.erase( F.clauses.begin() + i );
			
		} else {
			if (clauseWeights[i] < minClsWt){
				minClsWt = clauseWeights[i];
			}
			++i;
		}
	}
	assert(F.clauses.size() == clauseWeights.size());
	F.m = F.clauses.size() - 1;
	cout<<"minClsWt:"<<minClsWt<<endl;
}

void W2UDNFKLMCUSP::createClsBitMap(){
	uint64_t runningTot = 0;
	clsBitMap.push_back(-1); // 0th element is dummy in clauses and clauseWeights
	cout<<"Running tot:"<<endl;
	for (int i =1;i<clauseWeights.size();i++){
		clsBitMap.push_back(runningTot + (uint64_t)round(clauseWeights[i]/minClsWt));    //rounding error?
		runningTot += (uint64_t)round(clauseWeights[i]/minClsWt);
		cout<<runningTot<<" ";
	}
	clsHashWidth = ceil(log2(runningTot));
	cout<<endl<<"ClsHashWidth:"<<clsHashWidth<<endl;
}

int main(int argc, char* argv[]){
	
	if (!(argc==6)){
		cout<<"Usage: W2UDNFKLMCUSP <pivotApproxMC> <tApproxMC> <searchMode> <timeout> <input_wdnf_dimacs_file>"<<endl;
		exit(1);
	}
	DNFParser P;
	if(!P.parse_W2U_DNF_dimacs(argv[5])){
		cout<<"Could not parse dimacs file"<<endl;
		exit(1);
	}
	//RowEchelonHash assumps(20,30);
	W2UDNFKLMCUSP W2UDKC(P.getF(), P.getWeights(), P.getBitLen(), P.getClauseWeights(), P.getMaxClauseWt(), P.getTotClauseWt(), stoi(argv[1],NULL), stoi(argv[2],NULL), stoi(argv[3],NULL), stoi(argv[4],NULL));
	return W2UDKC.solve();	
}



