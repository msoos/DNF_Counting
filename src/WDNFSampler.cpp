/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
    
#include <iostream>
#include <vector>
#include <random>

#include "../include/WDNFSampler.h"

vector<bool> WDNFSampler::randomAssignment(){
	std::uniform_real_distribution<double_t> urd(0,U);
	double_t x = rb.getRandReal(urd);
	vector<double_t>::iterator pos = lower_bound(sampling_array.begin(),sampling_array.end(),x);
	//cout<<"Found lower bound"<<endl;
	int low = pos - sampling_array.begin();
	//randCls = low;
	string asmtBits = rb.GenerateRandomBits(F.n*bitLen);
	vector<bool> result(F.n);
	for (int i=0;i<F.n;i++){
		uint32_t varnum = 0;
		for(int j = 0; j<bitLen; j++){
			if (asmtBits[i*bitLen+j] =='1'){
				varnum += (1<<j);
			}
		}
		result[i] = (varnum < weights[i+1]);
	}	
	for(int j=0; j< F.clauses[low].size();j++){
		result[abs(F.clauses[low][j])-1]=(F.clauses[low][j]>0);
	}
	randClsNum = low;
	return(result);
}

void WDNFSampler::calculateFraction(uint64_t num, uint64_t den){
	double_t count = U*num/den;
	cout<<"Probability is "<<count<<endl;
}
