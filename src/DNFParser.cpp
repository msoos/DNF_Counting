/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
    
//#include <string>
#include <algorithm>
#include "../include/DNFParser.h"
#include <fstream>
#include <sstream>
#include <iostream>
#include <cassert>

using std::ifstream;
using std::cout;
using std::endl;
using std::stoi;
using std::stringstream;
bool less_vectors(const vector<int64_t>& a,const vector<int64_t>& b) {
   return a.size() < b.size();
}

bool less_clauses(const int64_t& a, const int64_t& b){
	return abs(a)<abs(b);
}

bool DNFParser::parse_DNF_dimacs(string file){
	ifstream inputFile;
	inputFile.open(file);
	
	if(!inputFile.is_open()){
		cout<<"Error opening file "<<file<<endl;
		return false;
	}
	
	vector<vector<int64_t>> clauses;
	uint64_t n,m,minClauseSize, maxClauseSize;
	string line;
	bool flag=false;
	int i = 0;
	vector<int64_t> a(0);
	while(getline(inputFile,line)){
		
		if(line[0]=='c'){
			continue;
		}
		if(line[0]=='p'){
			if(flag){
				continue;
			}
			char * end;
			n = strtol(line.c_str()+6,&end,10);	
			m = strtol(end, NULL, 10);
			flag=true;
			
			clauses.push_back(a);
			continue;
		}
		
		i++;
		if (i>m){
			cout<<"Number of clauses: "<<m<<" Number of lines: "<<n<<endl;
			return(false);
		}
		vector<int64_t> b(0);
		clauses.push_back(b);
		stringstream sline(line);
		int64_t lit;
		while (sline>>lit){
			if(lit==0){
				break;
			}
			clauses[i].push_back(lit);
		}
		sort(clauses[i].begin(), clauses[i].end(),less_clauses);
	}
	if (i<m){
		cout<<"Number of clauses: "<<m<<" Number of lines: "<<n<<endl;
		return(false);
	}
	
	sort(clauses.begin(),clauses.end(),less_vectors);
	minClauseSize = clauses[1].size();
	maxClauseSize = clauses[clauses.size()-1].size();
	inputFile.close();
	
	F = new DNFFormula(n,m,minClauseSize,maxClauseSize,clauses);
	
	return true;
}

bool DNFParser::parse_W2U_DNF_dimacs(string file){
	ifstream inputFile;
	inputFile.open(file);
	
	if(!inputFile.is_open()){
		cout<<"Error opening file "<<file<<endl;
		return false;
	}
	
	vector<vector<int64_t>> clauses;
	uint64_t n,m,minClauseSize, maxClauseSize=0;
	string line;
	bool flag=false;
	int i = 0;
	vector<int64_t> a(0); 	
	while(getline(inputFile,line)){
		
		if(line[0]=='c'){
			continue;
		}
		if(line[0]=='p'){
			if(flag){
				continue;
			}
			char * end;
			n = strtol(line.c_str()+6,&end,10);	
			m = strtol(end, &end, 10);
			bitLen = strtol(end, NULL, 10);
			flag=true;
			
			minClauseSize = n+1;
			clauses.push_back(a); // dummy cls and weights to make clause and variable numbering start from 1
			clauseWeights.push_back(0);
			weights.push_back(0); 
			for (int j = 0;j<n;j++){
				getline(inputFile,line);
				weights.push_back(stoi(line.c_str(),NULL));
			}
			continue;
		}
		
		i++;
		if (i>m){
			cout<<"Number of clauses: "<<m<<" Number of lines: "<<n<<endl;
			return(false);
		}
		vector<int64_t> b(0);
		clauses.push_back(b);
		
		stringstream sline(line);
		int64_t lit;
		double cls_wt = 1;
		while (sline>>lit){
			if(lit==0){
				break;
			}
			clauses[i].push_back(lit);
			if(lit<0){
				cls_wt *= (1 - weights[abs(lit)]/pow(2,bitLen));
			}
			else{
				cls_wt *= weights[abs(lit)]/pow(2,bitLen);
			}
		}
		clauseWeights.push_back(cls_wt);
		totClauseWt += cls_wt;
		if (cls_wt > maxClauseWt){
			maxClauseWt = cls_wt;
		}
		sort(clauses[i].begin(), clauses[i].end(),less_clauses);
		if (clauses[i].size() < minClauseSize){
			minClauseSize = clauses[i].size();
		}
	}
	if (i<m){
		cout<<"Number of clauses: "<<m<<" Number of lines: "<<n<<endl;
		return(false);
	}
	assert(clauses.size() == clauseWeights.size());
	assert(weights.size() == n+1);
	
	//sort(clauses.begin(),clauses.end(),less_vectors);
	//minClauseSize = clauses[1].size();
	inputFile.close();
	cout<<"Minclssize is "<<minClauseSize<<endl;
	//exit(1);
	F = new DNFFormula(n,m,minClauseSize,maxClauseSize,clauses);
	
	return true;
}

DNFFormula DNFParser::getF(){
	return *F;
}

uint32_t DNFParser::getBitLen(){
	return bitLen;
}

vector<uint32_t> DNFParser::getWeights(){
	return weights;
}

vector<double_t> DNFParser::getClauseWeights(){
	return clauseWeights;
}

double_t DNFParser::getMaxClauseWt(){
	return maxClauseWt;
}

double_t DNFParser::getTotClauseWt(){
	return totClauseWt;
}
