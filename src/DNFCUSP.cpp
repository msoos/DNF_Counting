/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
     
#include <iostream>


#include <limits>
#include "../include/DNFCUSP.h"
#include "../include/DNFParser.h"
#include "../include/Timers.h"
#include "../include/RandomBits.h"
#include <m4ri/m4ri.h>
using std::cout;
using std::endl;
using std::stoi;

double_t DNFCUSP::BoundedSATCount(double_t maxSolutions, bool upperBoundCheck){
	cout << "BoundedSATCount looking for " << maxSolutions << " solutions. Checking existing "<<solutions.size()<<" solutions.."<<std::flush;
	double logsols = log2(maxSolutions);
	if (((RowEchelonHash*)assumps)->inited){
		cout<<"inited so resetting doneCubes and clearing solutions!"<<endl;
		solutions.clear();
		doneCubes=1;
		((RowEchelonHash*)assumps)->inited=false;
	}
	if(((RowEchelonHash*)assumps)->subbed){
		cout<<"Subbed so resetting doneCubes!"<<endl;
		doneCubes=1;
	}
	if(((RowEchelonHash*)assumps)->flipped){
		cout<<"flipped so resetting doneCubes and clearing solutions!"<<endl;
		solutions.clear();
		doneCubes=1;
	}
	if((!((RowEchelonHash*)assumps)->subbed) && (!((RowEchelonHash*)assumps)->flipped)){
		int erased=0;
		for (auto it = solutions.begin(); it != solutions.end(); ){
			if (!((RowEchelonHash*)assumps)->checkAsnmt(*it)) {it = solutions.erase(it);erased++;}
			else ++it;
		}
		//cout<<"Erased "<<erased<<" solutions"<<endl;
		if(solutions.size()>maxSolutions){
			return maxSolutions+3;
		}
	}
	cout<<" currently have " <<solutions.size()<< " cubes done : " << doneCubes<<endl;
    for(; doneCubes<F.clauses.size();doneCubes++) {
		//cout<<"Donecubes:"<<doneCubes<<" #solutions "<<solutions.size()<<std::flush;
		uint64_t currHashCount = ((RowEchelonHash*)assumps)->RemoveAllConstraints();
		if ( (upperBoundCheck==true) && (currHashCount + log2(solutions.size()+1) > (F.n>logU?logU:F.n)) ){
			cout<<"UpperBound reached! Returning from BoundedSAT with "<< solutions.size()<<" solutions"<<endl;
			return solutions.size();
		}
		string clauseCons="";
		for(int j=0;j<F.clauses[doneCubes].size();j++){
			//cout<<"Abs="<<abs(F.clauses[i][j])-1<<endl;
			clauseCons += std::to_string(F.clauses[doneCubes][j])+=" ";
		}
		clauseCons += 	"0\n";
		//cout<<clauseCons<<endl;	
		((RowEchelonHash*)assumps)->AddConstraints(clauseCons);
		//cout<<"Added COnstraints. Simplifying"<<endl;
		int freevars = ((RowEchelonHash*)assumps)->simplify(maxSolutions);
		//cout<<"Simplified with rank = "<<freevars<<endl;
		if(freevars==-1){
			//cout<<"No solutions "<<std::flush;
			continue;
		}
		//cout<<"n = "<<n <<" rank= "<<rank<<endl;
		if (freevars > logsols) {
			//cout<<"freevars:"<<freevars<<" logsols:"<<logsols<<" freevars-logsols:"<<freevars-logsols<<" true?"<<(freevars-logsols>0)<<endl;
			return(maxSolutions+3);
		}
		
		bool last = false;
		while(true){
			//cout<<"Enumerated sols"<<endl;
			HashSolution*  hsln = ((RowEchelonHash*)assumps)->enumerateNextGraySoln(last);
			solutions.insert(*hsln);
			//cout<<"inserted solution"<<endl;
			delete hsln;
			//cout<<"deleted solution pts."<<endl;
			//cout<<" Solutions = "<<solutions.size()<<endl;
			if (solutions.size()>maxSolutions) {
				//cout<<"sol size="<<solutions.size()<<" greater than maxSols while enumerating"<<endl;
				return(maxSolutions+3);
			}
			if(last){
				break;
			}
		}
   }
    cout<<endl;
    if (solutions.size() > maxSolutions){
		cout<<"WARNING: Outside for loop in BSAT with num sols = "<<solutions.size()<<" and maxSols = "<<maxSolutions<<". Exiting.."<<endl;
		exit(1);
		//return(maxSolutions+3);
	}
	else return (solutions.size());
}
	

int DNFCUSP::solve(int seed){
	
	double startTime = cpuTimeTotal();
	/*cout<<"Clauses are:"<<endl;
	for( int i = 0;i<F.clauses.size();i++){
		for(int j =0; j<F.clauses[i].size();j++){
			cout<<F.clauses[i][j]<<" ";
		}
		cout<<endl;
	}*/
	//exit(1);
	
	SATCount solCount;
    cout<<"Starting search. pivot,tApproxMC,searchMode = "<<pivotApproxMC<<" "<<tApproxMC<<" "<<searchMode
    << " seed=" << seed
    <<endl;
    cout << "Using start iteration " << startIteration << endl;

    bool finished = false;
    if (searchMode == 0) {
        finished = ApproxMC(solCount);
    } else if (searchMode == 1){
        finished = ScalApproxMC(solCount);
    }
    else{
		finished = ReverseApproxMC(solCount);
	}
    if (!finished) {
        cout << " (TIMED OUT)" << endl;
        return 1;
    }

    if (solCount.hashCount == 0 && solCount.cellSolCount == 0) {
        cout << "The input formula is unsatisfiable." << endl;
        return 1;
    }

    /*if (conf.verbosity) {
        solver->print_stats();
    }*/

    cout << "Number of solutions is: " << solCount.cellSolCount
         << " x 2^" << solCount.hashCount << endl;

    double elapsedTime = cpuTimeTotal() - startTime;
	
	cout<<"DNFCUSP took "<<elapsedTime<<" seconds "<<endl;
	//cout<<"DNFCUSP final result for "<<":"<< F.n<<":"<<F.m<<":"<<elapsedTime<<endl;
	return 1;
}





