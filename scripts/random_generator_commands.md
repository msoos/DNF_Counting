# Timing experiments benchmarks:
./random_dnf_generator.py \
    --n "100000, 100001, 2" \
    --mdensity "2, 10, 2" \
    --noscaling \
    --msize "3, 50, 10" \
    --instances 20 random_dens_great_1/ --monotone

./random_dnf_generator.py 
    --n "100000, 100001, 2" \
    --density "0.1, 1, 0.2" \
    --noscaling \
    --msize "3, 50, 10" \
    --instances 20 random_dens_less_1/ --monotone

# Accuracy experiments benchmarks:

## dens_less_1
./random_dnf_generator.py \
    --n "100, 1000, 300" \
    --mdensity "0.3, 1, 0.2" \
    --scaling \
    --msize "0.03, 0.75, 0.16" \
    --instances 3 accurancy_exp/1 --monotone
./random_dnf_generator.py \
    --n "1000, 10000, 3000" \
    --mdensity "0.3, 1, 0.2" \
    --scaling \
    --msize "0.03, 0.37, 0.08" \
    --instances 3  accurancy_exp/2 --monotone

## dens_great_1
./random_dnf_generator.py \
    --n "100 1000 300" \
    --mdensity "1,9,2" \
    --scaling \
    --msize "0.03, 0.75, 0.16" \
    --instances 3 accurancy_exp/3 --monotone

./random_dnf_generator.py \
    --n "1000, 10000, 3000" \
    --mdensity "1, 9, 2" \
    --scaling \
    --msize "0.03, 0.37, 0.08" \
    --instances 3  accurancy_exp/4 --monotone
