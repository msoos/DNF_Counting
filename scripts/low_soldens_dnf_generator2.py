#monotone dnfs
import sys
import os
import random
import math


nLow = int(sys.argv[1])
nHigh =  int(sys.argv[2])
nStep = int(sys.argv[3])
mDensityLow = float(sys.argv[4])
mDensityHigh = float(sys.argv[5])
mDensityStep = float(sys.argv[6])
mSizeType = int(sys.argv[7])
mSizeLow = float(sys.argv[8])
mSizeHigh = float(sys.argv[9])
mSizeStep = float(sys.argv[10])
	
instances = int(sys.argv[11])

outputDir = sys.argv[12]

nsvLow = int(sys.argv[13])
nsvHigh = int(sys.argv[14])
nsvStep = int(sys.argv[15])

for n in range(nLow,nHigh,nStep):
	m = mDensityLow
	while m < mDensityHigh:
		mSizeLow1 = mSizeLow
		mSizeHigh1 = mSizeHigh
		mSizeStep1 = mSizeStep
		if mSizeType==0:
			mSizeLow1 = n*mSizeLow
			mSizeHigh1 = n*mSizeHigh
			mSizeStep1 = n*mSizeStep
			print n,mSizeLow,mSizeHigh,mSizeStep
		for k in range(int(mSizeLow1),int(mSizeHigh1),int(mSizeStep1)):	
			for numSampVars in range(int(nsvLow),int(nsvHigh),int(nsvStep)):
				for i in range(instances):
					allCubes = set([])
					#opStr = "p cnf "+str(n)+" "+str(int(n*m))+'\n'
					sampVars = set(random.sample(xrange(1,n+1),k))
					otherVars = set(xrange(1,n+1)) - sampVars
					j=0
					while True:
						cube = []
						for l in sampVars:
							#opStr += str(l)+" "
							cube += [l]
						#opStr += "0\n"
						cube.sort()
						allCubes.add(tuple(cube))
						j += 1
						removeVar = random.sample(sampVars,1)[0]
						sampVars.remove(removeVar)
						changeVars = random.sample(otherVars,numSampVars)
						for l1 in changeVars:
							cube = []
							for l2 in sampVars:
								#opStr += str(l2)+" "
								cube += [l2]
							#opStr += str(l1)+" 0\n"
							cube += [l1]
							cube.sort()
							allCubes.add(tuple(cube))
							j += 1
						l1 = random.sample(sampVars,1)[0]
						sampVars.remove(l1)
						sampVars.add(removeVar)
						l2 = random.sample(otherVars,1)[0]
						otherVars.remove(l2)
						otherVars.add(l1)
						sampVars.add(l2)
						if len(allCubes)>int(n*m):   #changed from j>int(n*m)
							break
					f = open(outputDir+"/prandomDNF_"+str(n)+"_"+str(int(n*m))+"_"+str(k)+"_"+str(i)+"_"+str(numSampVars)+".dnf",'w')
					f.write('p cnf '+str(n)+' '+str(int(n*m))+'\n')
					writtenCubes = 0
					numCubes = int(n*m)
					print numCubes, len(allCubes)
					for i in allCubes:
						for j in i:
							f.write(str(j)+' ')
						f.write('0\n')
						writtenCubes += 1
						if writtenCubes >= numCubes:
							break
					f.close()

		m += mDensityStep

