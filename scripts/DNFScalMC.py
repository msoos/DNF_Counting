import sys
import os
import math
import re
import random
import time
import math

def ensureDirectory(d):
    if not os.path.exists(d):
        os.makedirs(d)

def usage():
    usageStr = "Usage: python DNFScalMC.py [options] <inputFile> <outputFolder>\n"
    usageStr += "Generated samples (and log files if enabled) are written to <outputFolder>.\n"
    usageStr += "Options are entered as '-option=value' where 'option' is from the list below.\n"
    usageStr += "If an option is not specified, the default value given below is used.\n"
    usageStr += "\nBASIC OPTIONS:\n\n"
    usageStr += "samples: number of samples to generate (default: 1) \n"
    usageStr += "kappa: computed from desired tolerance as in Algorithm 1 of the TACAS-15 paper (default: 0.638, corresponding to epsilon=16)\n"
    usageStr += "threads: number of threads to use for sampling (default: OpenMP system default)\n"
    usageStr += "timeout: overall timeout in seconds (default: 72000)\n"
    usageStr += "satTimeout: timeout for a single BoundedWeightSAT call in seconds (default: 3000)\n"
    usageStr += "runIndex: number appended to names of output & log files (default: current time) \n"
    usageStr += "\nADVANCED OPTIONS (not needed for typical use):\n\n"
    usageStr += "logging: 0 to turn off logging and 1 to turn it on. If logging is turned on, the log file(s) will be present in <outputFolder>/logging\n"
    usageStr += "aggregateSolutions: 0/1 to disable/enable merging samples from all threads (default: 1)\n"
    usageStr += "writeSamples: 0/1 to disable/enable writing samples to a file (default: 1)\n"
    usageStr += "multisampling: 0/1 to disable/enable returning multiple samples from each UniGen2 call (default: 1)\n"
    usageStr += "startIteration: initial number of XOR clauses to use in UniGen2, or 0 if this should be computed using sharpSAT/ApproxMC, or -1 if it should be computed using ApproxMC only (e.g. when doing projection counting) (default: 0)\n"
    usageStr += "callsPerSolver: number of UniGen2 calls to make in a single solver (without clearing learned clauses), or 0 to use a built-in heuristic (default: 0)\n"
    usageStr += "pivotAC: pivot value for ApproxMC (default: 60)\n"
    usageStr += "tApproxMC: number of iterations for ApproxMC (default: 1)\n"
    usageStr += "xorFactor: factor (>1) by which to multiply minimum allowed probability by for selecting variables in xors (default:1.2) \n"
    print usageStr
    exit(1)

def getInputs():
    paramMap={}
    action=0
    error = ''
    gotInputFile = False
    acceptedParams=['timeout','satTimeout','approxMCSearchMode','runIndex','samples','callsPerSolver','writeSamples','threads','aggregateSolutions','logging','kappa','multisampling','startIteration','pivotAC','tApproxMC','xorFactor']
    for i in range(1,len(sys.argv)):
        if (not(sys.argv[i][0] == '-')):
            if not gotInputFile:
                paramMap['inputFile'] = sys.argv[i]
                gotInputFile = True
            else:
                paramMap['outputFolder'] = sys.argv[i]
                action = 3
                return action,error,paramMap
        else:
            if (sys.argv[i][1] == 'h'):
                action = 0
                return action,error,paramMap
            fieldValues = sys.argv[i][1:].strip().split('=')
            if (not(fieldValues[0] in acceptedParams)):
                action = 1
                error = "Could not understand the option "+str(fieldValues[0])+"\n"
                return action,error,paramMap
            else:
                paramMap[fieldValues[0]] = fieldValues[1]
    action = 2
    error = "You must specify an input file and an output folder.\nUse the option -h for help.\n"
    return action,error,paramMap

def dnf_to_cnf(inputFile, outputFile):
    f1=open(inputFile,'r')
    f2=open(outputFile,'w')
    
    i=0
    num_literals=0
    output=""
    for line in f1:
        #print str(i)+':'+line
        if line[0]=='c':
            #f2.write(line);
            continue;
        i+=1
        if i==1:
            words=line.split()
            nbvars=int(words[2])
            
            #write sample set to cnf file in c ind format
            k=0
            while True:
                k += 1
                f2.write("c ind ")
                while True:
                    f2.write(str(k)+" ")
                    if k%10==0 or k==nbvars:
                        f2.write('0\n')
                        break
                    k += 1
                if k==nbvars:
                    break
            nbclauses=int(words[3])
            minClauseLength = nbvars
            newVars=nbvars+nbclauses
            continue;
        clause_vars=line.split()
        num_literals=num_literals+len(clause_vars)-1
        if num_literals < minClauseLength:
            minClauseLength = num_literals
        for j in range(len(clause_vars)-1):
            output=output+"-"+str(nbvars+i-1)+" "+clause_vars[j]+" 0\n"
        for j in range(len(clause_vars)-1):
            output=output+str(-int(clause_vars[j]))+" "
        output=output+str(nbvars+i-1)+' 0\n'
    if i-1!=nbclauses:
        print "Error: Clauses=",str(nbclauses)," lines=",str(i)
        exit(1)
    f2.write("p cnf "+str(newVars)+" "+str(num_literals+1+nbclauses)+"\n")
    for j in range(nbvars+1,nbvars+nbclauses+1):
        f2.write(str(j)+" ")
    f2.write("0\n")
    f2.write(output)
    f2.close()
    return nbvars, math.pow(2,nbvars-minClauseLength)


def choose(n, k):
    if 0 <= k <= n:
        ntok = 1
        ktok = 1
        for t in xrange(1, min(k, n - k) + 1):
            ntok *= n
            ktok *= t
            n -= 1
        return ntok // ktok
    else:
        return 0

def wstar(n, q):
    if q == 0:
        raise RuntimeError('w*: Lower bound must be > 0')
    if q > 2**n + 1:
        raise RuntimeError('w*: Lower bound must be <= 2**n + 1')
    j = 0
    tot = 0
    while tot <= q - 1:
        j += 1
        tot += choose(n, j)
    return j - 1

def r(n, q, wstar_val=None):
    if not wstar_val:
        wstar_val = wstar(n, q)
    tot = q - 1
    for w in xrange(1, wstar_val + 1):
        tot += choose(n, w)
    return tot

def compute_ep_factor(numvar, numxor, numsol_lowerbound, density):
    """
    Given
        numvar: the number of variables,
        numxor: the number of xor constraints,
        numsol_lowerbound: a lower bound on the number of solutions, and
        density: the density of xor constraints to use,
    Compute the scaling factor applied to the pivot used for ApproxMC
    """
    wstar_val = wstar(numvar, numsol_lowerbound)
    x = 1 - 2*density
    B1 = 2**numxor
    B2 = numsol_lowerbound - 1
    for w in xrange(1, wstar_val+1):
        B1 += choose(numvar, w) * (1 + x**w)**numxor
        B2 -= choose(numvar, w)
    B2 *= (1 + x**(wstar_val+1))**numxor
    return (B1 + B2 - numsol_lowerbound) / float(2**numxor)


def main():
    runIndex = str(int(time.time()))
    timeout = 72000
    satTimeout = 3000
    initialFileName = ''
    numVariables = 0
    numClauses = 0
    shouldLog = True
    outputFileName = ''
    logFileName = ''
    epsilon = 0
    pivotUniGen = 0
    kappa = 0.638
    samples = 1
    writeSamples = True
    multisampling = True
    threads = 0
    aggregateSolutions = True
    xorDensityParameter = 0
    pivotAC = 60
    approxMCIterations = 7
    startIteration = 0
    callsPerSolver = 0
    approxMCSearchMode = 0;
    xorFactor=1.9
    action,error,paramMap = getInputs()
    if (action == 0):
        usage()
        exit(1)
    if (action == 1 or action == 2):
        print error
        exit(1)
    if (paramMap.has_key('approxMCSearchMode')):
        approxMCSearchMode = int(paramMap['approxMCSearchMode'])
    if (paramMap.has_key('runIndex')):
        runIndex = int(paramMap['runIndex'])
    if (paramMap.has_key('samples')):
        samples = int(paramMap['samples'])
    if (paramMap.has_key('callsPerSolver')):
        callsPerSolver = int(paramMap['callsPerSolver'])
    if (paramMap.has_key('writeSamples')):
        writeSamples = (paramMap['writeSamples'] == '1')
    if (paramMap.has_key('timeout')):
        timeout = float(paramMap['timeout'])
    if (paramMap.has_key('satTimeout')):
        satTimeout = float(paramMap['satTimeout'])
    if (paramMap.has_key('kappa')):
        kappa = float(paramMap['kappa'])
    if (paramMap.has_key('outputFile')):
        outputFileName = paramMap['outputFile']
    if (paramMap.has_key('logging')):
        if (paramMap['logging'] == '0'):
            shouldLog = False
        if (paramMap['logging'] == '1'):
            shouldLog = True
    if (paramMap.has_key('multisampling')):
        if (paramMap['multisampling'] == '0'):
            multisampling = False
        if (paramMap['multisampling'] == '1'):
            multisampling = True
    if (paramMap.has_key('aggregateSolutions')):
        if (paramMap['aggregateSolutions'] == '0'):
            aggregateSolutions = False
        if (paramMap['aggregateSolutions'] == '1'):
            aggregateSolutions = True
    if (paramMap.has_key('threads')):
        threads = int(paramMap['threads'])
    if (paramMap.has_key('pivotAC')):
        pivotAC = int(paramMap['pivotAC'])
    if (paramMap.has_key('tApproxMC')):
        approxMCIterations = int(paramMap['tApproxMC'])
    if (paramMap.has_key('startIteration')):
        startIteration = int(paramMap['startIteration'])
    if (paramMap.has_key('xorFactor')):
        xorFactor = float(paramMap['xorFactor'])

    initialFileName = paramMap['inputFile']
    outputFolder = paramMap['outputFolder']
    initialFileNameSuffix =  initialFileName.split('/')[-1][:-4]
    ensureDirectory(outputFolder)
    cnfFileName = outputFolder+"/"+str(initialFileNameSuffix)+'.cnf'
    sample_set, minSolutions = dnf_to_cnf(initialFileName,cnfFileName)
    xorDensity = math.log(sample_set,2)*xorFactor/sample_set
    print 'xorDensity: '+str(xorDensity)
    numxor = math.log(minSolutions,2) - math.log(2*pivotAC,2) - 1
    if (shouldLog):
        ensureDirectory(outputFolder+"/logging/")
        logFilePrefix = outputFolder+"/logging/"+str(initialFileNameSuffix)+'_'+str(runIndex)
    outputFileName = outputFolder+"/"+str(initialFileNameSuffix)+'_'+str(runIndex)+".txt"

    pivotUniGen = math.ceil(4.03*(1+1/kappa)*(1+1/kappa))

#    if startIteration < 0:
#        startIteration = 0
#        print "Doing projection counting; will use ApproxMC"
#    elif startIteration == 0:
#        startIteration = 0
#        print "Attempting to compute startIteration with sharpSAT"
#        countLogFile = outputFolder+"/"+initialFileNameSuffix+"_"+str(runIndex)+".count"
#        cmd = "./doalarm -t real 300 ./sharpSAT -q "+initialFileName+" > "+ countLogFile
#        print cmd
#        os.system(cmd)
#        f = open(countLogFile,'r')
#        lines = f.readlines()
#        f.close()
#        failed = True
#        if (len(lines) > 1):
#            if (lines[1].strip() == "END"):
#                failed = False
#                count = long(lines[0].strip())
#                logCount = math.log(count, 2)
#                startIteration = int(round(logCount + math.log(1.8, 2) - math.log(pivotUniGen, 2))) - 2
#                #print "Solution count estimate is %f * 2^%d" % (count / (2.0 ** int(logCount)), int(logCount))
#        if failed:
#            print "sharpSAT failed or timed out; using ApproxMC instead"
#        else:
#            print "startIteration computed successfully"
    
    pivotFactor = compute_ep_factor(sample_set, numxor, minSolutions, xorDensity) 
    print pivotFactor,sample_set,numxor,minSolutions,xorDensity
    assert (pivotFactor <= 1.3),"computed pivotFactor "+str(pivotFactor)+" is greater than 1.3!"
    cmd = "ScalMCCode/build/scalmc --samples="+str(samples)+" --kappa="+str(kappa)+" --pivotUniGen="+str(pivotUniGen)+" --maxTotalTime="+str(timeout)+" --startIteration="+str(max(startIteration,numxor))+" --maxLoopTime="+str(satTimeout)+" --tApproxMC="+str(approxMCIterations)+" --pivotAC="+str(pivotAC*pivotFactor)+" --gaussuntil=400 --verbosity=0"
    if (shouldLog):
        cmd += " --logFile="+logFilePrefix
    if approxMCSearchMode > 0:
        cmd += " --approxMCSearchMode="+str(approxMCSearchMode)
    if callsPerSolver > 0:
        cmd += " --callsPerSolver="+str(callsPerSolver)
    if multisampling:
        cmd += " --multisample"
    if not writeSamples:
        cmd += " --nosolprint"
    if threads > 0:
        cmd += " --threads="+str(threads)
    if not aggregateSolutions:
        cmd += " --noAggregation"
    cmd += " --xorDensity="+str(xorDensity)
    #cmd += " --minSolutions="+str(minSolutions)
    cmd += " "+cnfFileName+" "+outputFileName
    print cmd
    sys.stdout.flush()
    os.system(cmd)

main()    
