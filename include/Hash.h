/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
    
#ifndef HASH_H_
#define HASH_H_

class Hash {
public:
	Hash(int nVars) : numVars ( nVars ){}
	virtual void initHash(uint32_t numStartHashes) = 0;
	virtual bool AddHash(uint32_t num_xor_cls) = 0;
	virtual bool SetHash(uint32_t num_xor_cls) = 0;
	virtual bool SubHash(uint32_t num_xor_cls) = 0;
	virtual bool AddFlippedHash() = 0;
	virtual bool SubFlippedHash() = 0;
	//virtual bool clear(bool init) = 0;
	
protected:
	uint64_t currHashCount;
	const uint32_t numVars;
};
#endif
