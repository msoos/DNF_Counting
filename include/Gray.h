/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
    
#ifndef GRAY_H_
#define GRAY_H_

#include <vector>
#include <iostream>

using std::vector;
class Gray{
	private:
		uint32_t numBits;
		vector<bool> a;
		vector<uint32_t> f;
	public:
		Gray(uint32_t numBits_): numBits(numBits_){
			a = vector<bool>(numBits);
			f = vector<uint32_t>(numBits+1);
			for (int i = 0; i<numBits;i++){
				a[i] = false;
				f[i] = i;
			}
			f[numBits] = numBits;
		}
		
		uint32_t getNextPosition();
};

#endif
