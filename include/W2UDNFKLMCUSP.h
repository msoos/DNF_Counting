/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
    
#ifndef W2UDNFKLMCUSP_H_
#define W2UDNFKLMCUSP_H_

#include <iostream>
#include "DNFFormula.h"
#include "CUSP.h"
#include "RowEchelonHash.h"
#include "DNFKLMCUSP.h"

class W2UDNFKLMCUSP: public DNFKLMCUSP{

	public:
		W2UDNFKLMCUSP(DNFFormula F_, vector<uint32_t> weights_, uint32_t bitLen_, vector<double_t> clauseWeights_, double_t maxClauseWt_, double_t totClauseWt_, uint32_t pivotApproxMC_, uint32_t tApproxMC_, uint32_t searchMode_, uint32_t loopTimeout_)
		: DNFKLMCUSP(F_, pivotApproxMC_, tApproxMC_, searchMode_, loopTimeout_), weights(weights_), bitLen(bitLen_), clauseWeights(clauseWeights_), maxClauseWt(maxClauseWt_), totClauseWt(totClauseWt_){
			eps = 0.8; // TODO: derive from pivotApproxMc
			//filterClauses(maxClauseWt * eps / F.m);
			filterClauses(0);
			createClsBitMap();
			logm = (uint32_t)ceil(log2(F_.m));
			logpivot = (uint32_t)ceil(log2(pivotApproxMC_));
			numHashVars = (F.n-F.minClauseSize)*bitLen+clsHashWidth;
			assumps = new RowEchelonHash(logm+logpivot,numHashVars);         // still using logm+logpivot free vars since each enumerated assignment will have at least 1/m count
			startIteration = numHashVars - logm - logpivot;
			endIteration = numHashVars - logpivot;
			std::cout<<"StartIteration in constructor:"<<startIteration<<std::endl;
			
		}
			
		int solve() override;
	
	protected:
		//int64_t BoundedSATCount(uint32_t maxSolutions) override;
		void filterClauses(double_t);
		void createClsBitMap();
		int getSigma(vector<bool> hashSoln, vector<bool>& sigma) override;
		
		//uint64_t runningTot = 0;
		vector<uint32_t> weights;
		vector<double_t> clauseWeights;
		uint32_t bitLen;
		double_t maxClauseWt, minClsWt, totClauseWt;
		uint32_t clsHashWidth;
		vector<int64_t> clsBitMap;
		double eps; // required for filter clauses
};

#endif
