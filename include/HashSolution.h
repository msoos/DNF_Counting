/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
    
#ifndef HashSolution_H_
#define HashSolution_H_

#include <m4ri/m4ri.h>
#include <vector>

using std::vector;
class HashSolution{
	public:
		HashSolution(mzd_t* a, const uint64_t hash_) : asnmt(a), hash(hash_){
		}
		~HashSolution(){
			mzd_free(asnmt);
		}
		HashSolution(const HashSolution& hs): hash(hs.hash){
			asnmt=mzd_copy(NULL,hs.asnmt);
		}
		inline mzd_t* getAsnmt() const{
			return asnmt;
		}
		inline uint64_t getHash() const{
			return mzd_hash(asnmt);
		}
		inline bool equals(HashSolution hs) const{
			return mzd_equal(asnmt,hs.asnmt);
		}
		
		inline vector<bool> getVector() const{
			vector<bool> a = vector<bool>(asnmt->ncols);
			for(int i = 0; i<asnmt->ncols;i++){
				a[i] = mzd_read_bit(asnmt,0,i);
			}
			return a;
		}
	private:
		mzd_t *asnmt;
		const uint64_t hash;
		HashSolution& operator=( const HashSolution& hs){
			return *this;
		}
};

#endif
