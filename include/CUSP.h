/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 * Copyright (c) 2016, 2013, Supratik Chakraborty, Kuldeep S. Meel, Moshe Y. Vardi
 *  
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
    */
     

#ifndef CUSP_H_
#define CUSP_H_

#include "Hash.h"
#include <vector>
#include <algorithm>
#include <fstream>
#include <string>
#include <cmath>

using std::vector;

struct SATCount {
    void clear()
    {
        SATCount tmp;
        *this = tmp;
    }
    uint32_t hashCount = 0;
    uint32_t cellSolCount = 0;
};

template<class T>
inline T findMedian(vector<T>& numList)
{
    std::sort(numList.begin(), numList.end());
    size_t medIndex =  numList.size() / 2;  //removed plus 1
    size_t at = 0;
    if (medIndex >= numList.size()) {
        at += numList.size() - 1;
        return numList[at];
    }
    at += medIndex;
    return numList[at];
}

template<class T>
inline T findMin(vector<T>& numList)
{
    T min = std::numeric_limits<T>::max();
    for (const auto a: numList) {
        if (a < min) {
            min = a;
        }
    }
    return min;
}

class CUSP{
	
public:
	virtual int solve(int seed) = 0;
	CUSP(uint32_t pivotApproxMC_=74, 	uint32_t tApproxMC_=9, uint32_t searchMode_=1, uint32_t loopTimeout_=2500, std::string cuspLogFile = "cusp_log.txt")
	: pivotApproxMC(pivotApproxMC_), tApproxMC(tApproxMC_), searchMode(searchMode_), loopTimeout(loopTimeout_) {
		openLogFile(cuspLogFile);
	}
	~CUSP(){
		cusp_logf.close();
		std::cout<<"Closed cusplog"<<std::endl;
	}
protected:
	bool openLogFile(std::string cuspLogFile);
	bool ScalApproxMC(SATCount& count);
	bool DNFScalApproxMC(SATCount& count);
    bool ApproxMC(SATCount& count);
	bool ReverseApproxMC(SATCount& count);
	
    virtual double_t BoundedSATCount(double_t maxSolutions, bool upperBoundCheck) = 0; //made double to support reverseApproxMC when using stochastic counting
		
	Hash* assumps;
	uint32_t numHashVars;
    uint32_t startIteration;
    uint32_t endIteration;
    uint32_t pivotApproxMC;
    uint32_t tApproxMC;
    uint32_t searchMode;
    double   loopTimeout;
    std::ofstream cusp_logf;
    
};

#endif
