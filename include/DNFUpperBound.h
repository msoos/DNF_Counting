/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
    
#ifndef DNFUPPERBOUND_H_
#define DNFUPPERBOUND_H_

#include <gmp.h>
#include <gmpxx.h>
#include <iostream>
#include <iomanip>

#include "DNFFormula.h"

using std::cout;
using std::endl;

static inline void fp_epsilon (mpf_t eps, uint32_t prec);
static inline void fp_log2 (mpf_t l2, uint32_t prec);
static inline void fp_log (mpf_t lg, const mpf_t z, uint32_t prec);

static inline double_t getLogU(DNFFormula F){
	if(F.minClauseSize==F.maxClauseSize){
		std::cout<<"Uniform cube size."<<std::endl;
		std::cout<<"logU is "<<std::setprecision(17)<<log2(F.m)+F.n-F.minClauseSize<<std::setprecision(2)<<std::endl;
		return log2(F.m)+F.n-F.minClauseSize;
	}
	else{
		std::cout<<"Non uniform cube size"<<std::endl;
		mpz_t temp;
		mpz_init(temp);
		mpz_t U;
		mpz_init(U);
		for (int i = 1; i<= F.m;i++){
			mpz_ui_pow_ui(temp,2,F.n-F.clauses[i].size());
			mpz_add(U,U,temp);
		}
		//gmp_printf("U: %Zd\n",U);
		mpz_clear(temp);
		mpf_t mylog, fU;
		int nBits, decimal_prec;
		decimal_prec = 15;
		nBits = 3.3* decimal_prec;
		nBits += nBits/2.0; //padding
		mpf_set_default_prec(nBits);
		
		mpf_init(mylog);
		mpf_init(fU);
		mpf_set_z(fU,U);
		
		fp_log(mylog,fU,decimal_prec);
		mpf_t ln2;
		mpf_init(ln2);
		fp_log2(ln2,decimal_prec);
		mpf_div(mylog,mylog,ln2);
		//gmp_printf("U is 2^%.*Ff",decimal_prec, mylog);
		//cout<<endl;
		cout<<"logU is "<<std::setprecision(17)<<mpf_get_d(mylog)<<std::setprecision(2)<<std::endl;
		return mpf_get_d(mylog);
	}
}

// following taken from https://github.com/linas/anant/tree/master/src
// int and unsigned int changed to uint32_t

static inline void fp_epsilon (mpf_t eps, uint32_t prec)
{
	static int cache_prec = -1;
	static mpf_t cache_eps;

	if (-1 == cache_prec)
	{
		mpf_init (cache_eps);
	}

	if (prec == cache_prec)
	{
		mpf_set (eps, cache_eps);
		return;
	}
	if (cache_prec < prec)
	{
		mpf_set_prec (cache_eps, 3.322*prec+50);
	}

	/* double mex = ((double) prec) * log (10.0) / log(2.0); */
	double mex = ((double) prec) * 3.321928095;
	unsigned int imax = (unsigned int) (mex +1.0);
	mpf_t one;
	mpf_init (one);
	mpf_set_ui (one, 1);
	mpf_div_2exp (cache_eps, one, imax);

	mpf_set (eps, cache_eps);
	cache_prec = prec;
	mpf_clear (one);
}

static inline void fp_log_m1 (mpf_t lg, const mpf_t z, uint32_t prec)
{
	mp_bitcnt_t bits = ((double_t) prec) * 3.322 + 50;
	mpf_t zee, z_n, term;

	mpf_init2 (zee, bits);
	mpf_init2 (z_n, bits);
	mpf_init2 (term, bits);

	/* Make copy of argument now! */
	mpf_set (zee, z);
	mpf_mul (z_n, zee, zee);
	mpf_set (lg, zee);

	/* Use 10^{-prec} for smallest term in sum */
	mpf_t maxterm;
	mpf_init2 (maxterm, bits);
	fp_epsilon (maxterm, prec);

	uint32_t n=2;
	while(1)
	{
		mpf_div_ui (term, z_n, n);
		mpf_add (lg, lg, term);

		/* don't go no farther than this */
		mpf_abs (term, term);
		if (mpf_cmp (term, maxterm) < 0) break;

		n ++;
		mpf_mul (z_n, z_n, zee);
	}

	mpf_clear (zee);
	mpf_clear (z_n);
	mpf_clear (term);

	mpf_clear (maxterm);
}

static inline void fp_log2 (mpf_t l2, uint32_t prec)
{
	// lines not commented since static required since mutually recursive with fp_log
	static uint32_t precision=0;
	static mpf_t cached_log2;

	//pthread_spin_lock(&mp_const_lock);
	if (precision >= prec)
	{
		mpf_set (l2, cached_log2);
		//pthread_spin_unlock(&mp_const_lock);
		return;
	}

	if (0 == precision)
	{
		mpf_init (cached_log2);
	}
	mp_bitcnt_t bits = ((double_t) prec) * 3.322 + 50;
	mpf_set_prec (cached_log2, bits);

	mpf_t two;
	mpf_init2 (two, bits);
	mpf_set_ui (two, 2);
	fp_log (cached_log2, two, prec);
	mpf_set (l2, cached_log2);

	mpf_clear (two);
	precision = prec;
	//pthread_spin_unlock(&mp_const_lock);
}

static inline void fp_log (mpf_t lg, const mpf_t z, uint32_t prec)
{
	mp_bitcnt_t bits = ((double_t) prec) * 3.322 + 50;
	mpf_t zee;
	mpf_init2 (zee, bits);
	mpf_set (zee, z);
	int32_t nexp = 0;

	/* Assume branch cut in the usual location, viz
	 * along negative z axis */
	if (mpf_cmp_ui(zee, 0) <= 0)
	{
		fprintf(stderr, "Error: bad domain for fp_log: log(%g)\n",
			mpf_get_d(zee));
		mpf_clear (zee);
		exit (1);
	}

	/* Find power of two by means of bit-shifts */
	while (mpf_cmp_ui(zee, 2) > 0)
	{
		nexp ++;
		mpf_div_ui (zee, zee, 2);
	}

	while (mpf_cmp_ui(zee, 1) < 0)
	{
		nexp --;
		mpf_mul_ui (zee, zee, 2);
	}

	/* Apply simple-minded series summation
	 * This appears to be faster than the Feynman algorithm
	 * for the types of numbers and precisions I'm encountering. */
	if (mpf_cmp_d(zee, 1.618) > 0)
	{
		mpf_ui_div (zee, 1, zee);
		mpf_ui_sub (zee, 1, zee);
		fp_log_m1 (lg, zee, prec);
	}
	else
	{
		mpf_ui_sub (zee, 1, zee);
		fp_log_m1 (lg, zee, prec);
		mpf_neg (lg, lg);
	}

	/* Add log (2^n) = n log (2) to result. */
	if (0 != nexp)
	{
		fp_log2 (zee, prec);
		if (0 > nexp)
		{
			mpf_mul_ui (zee, zee, -nexp);
			mpf_neg (zee, zee);
		}
		else
		{
			mpf_mul_ui (zee, zee, nexp);
		}
		mpf_add (lg,lg, zee);
	}

	mpf_clear (zee);
}


#endif
