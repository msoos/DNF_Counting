/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 * Copyright (C) 2005 Linas Vepstas
 *  
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
/*
 * See https://github.com/linas/anant for the original implementation of the log function (adapted here) and many others.
 */
   
#ifndef KLSAMPLER_H_
#define KLSAMPLER_H_

#include <cstddef>
#include <gmp.h>
#include <gmpxx.h>
#include <ctime>
#include <iostream>
#include <vector>

#include "Sampler.h"
#include "DNFFormula.h"
#include "RandomBits.h"

using std::vector;
class KLSampler : public Sampler{
	public:
		KLSampler(DNFFormula F_, int seed=-1):
		Sampler (F_){
			if(F.minClauseSize==F.maxClauseSize){
				std::cout<<"Uniform cube size."<<std::endl;
                if (seed == -1) {
                    rb.SeedEngine();
                } else {
                    rb.SeedFixed(seed);
                }
				uniform=true;
			}
			else{
				std::cout<<"Non uniform cube size"<<std::endl;
				uniform = false;
				sampling_array.resize(F.m+1);
				mpz_t temp;
				mpz_init(temp);
				for (int i = 1; i<= F.m;i++){
					mpz_ui_pow_ui(temp,2,F.n-F.clauses[i].size());
					mpz_add(temp,sampling_array[i-1].get_mpz_t(),temp);
					mpz_set(sampling_array[i].get_mpz_t(),temp);
				}
				mpz_init(U);
				mpz_set(U,sampling_array[F.m].get_mpz_t());
				gmp_printf("U: %Zd\n",U);
				mpz_clear(temp);
		
				gmp_randinit_mt (state1);
				gmp_randseed_ui(state1, std::random_device{}());
				
				mpz_init(randNum_clause);
				mpz_init(randNum_assnmnt);
				
				gmp_randinit_mt (state2);
				gmp_randseed_ui(state2, std::random_device{}());
				//mpz_init(clsNum);
				//mpz_init(tempClsNum);
			}
		}
		vector<bool> randomAssignment() override;
		void calculateFraction(double_t num, uint64_t den) override;
		uint32_t getClauseFromBits(vector<bool>);
		double_t getLogU();
		uint32_t randClsNum;
	protected:
		void fp_epsilon (mpf_t eps, uint32_t prec);
		void fp_log_m1 (mpf_t lg, const mpf_t z, uint32_t prec);
		void fp_log2 (mpf_t l2, uint32_t prec);
		void fp_log (mpf_t lg, const mpf_t z, uint32_t prec);
		
		bool uniform;
		vector<mpz_class> sampling_array;
		mpz_t U;
		mpz_t randNum_clause; 
		mpz_t randNum_assnmnt;
		mpz_t clsNum, tempClsNum;
		double_t logU;
		gmp_randstate_t state1;
		gmp_randstate_t state2;
		RandomBits rb;
		
};
#endif
