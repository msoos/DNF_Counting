/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
    
#ifndef DNFKLM_H_
#define DNFKLM_H_

#include "KLSampler.h"
#include "DNFParser.h"
#include "DNFFormula.h"
#include "RandomBits.h"

struct SampleCount{
	uint64_t num;
	uint64_t den;
};

class DNFKLM{
	public:
		DNFKLM(DNFFormula F_, double_t eps_, double_t delta_, int seed = -1):
		eps(eps_), delta(delta_), F(F_){
			DS = new KLSampler(F, seed);
            if (seed == -1) {
                rb.SeedEngine();
            } else {
                rb.SeedFixed(seed);
            }
			
		}
		int solve();
					
	protected:
		void self_adjust_coverage_thm2();
		
		
		bool satisfies(vector<bool> s, uint64_t clsnum);
		
		KLSampler *DS;
		DNFFormula F;
		double_t eps, delta, mu, rho;
		SampleCount count;
		
		RandomBits rb;
		std::uniform_int_distribution<uint64_t> uid {1,F.m};
};

#endif
