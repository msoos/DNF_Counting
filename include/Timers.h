/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 * Copyright (c) 2016, 2013, Supratik Chakraborty, Kuldeep S. Meel, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
    
#ifndef TIMERS_H_
#define TIMERS_H_

#include <ctime>
#include <chrono>
#include <sys/resource.h>

static inline double cpuTimeTotal(void){
	struct rusage ru;
	getrusage(RUSAGE_SELF,&ru);
	//cout<<"User time: "<<ru.ru_utime.tv_sec<<" Sys time: "<<ru.ru_stime.tv_sec<<endl;
	return (double) ru.ru_utime.tv_sec+(double) ru.ru_utime.tv_usec/1000000.0 + (double) ru.ru_stime.tv_sec+(double) ru.ru_stime.tv_usec/1000000.0;
}

static inline void print_wall_time (void)
{
    auto time_now = std::chrono::system_clock::now();
    std::time_t end_time = std::chrono::system_clock::to_time_t(time_now);
    std::cout << std::ctime(&end_time);
}

#endif
