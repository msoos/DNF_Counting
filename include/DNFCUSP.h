/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
    
#ifndef DNFCUSP_H_
#define DNFCUSP_H_

#include <iostream>
#include <unordered_set>
#include "DNFFormula.h"
#include "CUSP.h"
#include "RowEchelonHash.h"
#include "HashSolution.h"
//#include "DNFUpperBound.h"

using std::unordered_set;

struct myhash {
public:
	size_t operator()(const HashSolution & s) const {
		uint64_t hash_ = s.getHash();
		return std::hash<int>()(hash_);
	}
};
 
// Custom comparator that compares the string objects by length
struct myequal {
public:
	bool operator()(const HashSolution & s1, const HashSolution & s2) const {
 
		return(s1.equals(s2));
	}
};

class DNFCUSP: public CUSP{

	public:
		DNFCUSP(DNFFormula F_, double_t eps_, uint32_t pivotApproxMC_, 	uint32_t tApproxMC_, uint32_t searchMode_, uint32_t loopTimeout_, std::string cuspLogFile_, int seed)
		: CUSP(pivotApproxMC_, tApproxMC_, searchMode_, loopTimeout_, cuspLogFile_), F(F_), eps(eps_) {
			
			logm = (uint32_t)ceil(log2(F_.m));
			logU = F.n - F.minClauseSize + logm;
			//logU = getLogU(F);
			//std::cout<<"LogU: "<<logU<<std::endl;
			logpivot = (uint32_t)ceil(log2(pivotApproxMC_));
			startIteration = F.n - F.minClauseSize - logpivot;
			endIteration = F.n-logpivot+2;
			assumps = new RowEchelonHash(F.n-startIteration,F.n, F.maxClauseSize, seed);
			numHashVars = F.n;
			doneCubes = 1;
			std::cout<<"StartIteration in constructor:"<<startIteration<<std::endl;
			std::cout<<"endIteration in constructor:"<<endIteration<<std::endl;
		}
			
		int solve(int seed) override;
	
	protected:
		double_t BoundedSATCount(double_t maxSolutions, bool upperBoundCheck) override;
		
		unordered_set<HashSolution,myhash,myequal> solutions;		
		DNFFormula F;
		uint64_t doneCubes;
		uint32_t logpivot, logm;
		double_t eps, logU;
};
#endif
