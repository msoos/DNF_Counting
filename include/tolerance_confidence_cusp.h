/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
    
#ifndef TOLERANCE_COFIDENCE_H_
#define TOLERANCE_COFIDENCE_H_

#include <iostream>
using std::cout;
using std::endl;


static inline int32_t getPivot(double_t eps){
	//taken from ApproxMC on bitbucket Main.cpp
	return int32_t(1 + 9.84*(1+(1/eps))*(1+(1/eps))*(1+(eps/(1+eps))));
}

static inline int32_t getT(double_t delta){
	//taken from prob_map_file_36.txt
	double_t probs[] = {0.64,0.4096,0.704512,0.54525952,0.7491026944,0.62679678976,0.783348347699,0.684720866198,0.81096404252,0.729158464263,0.833869604432,0.76476025192,0.853220223135,0.794078413808,0.869779929746,0.818681406488,0.884087516258,0.839611361615,0.896540839559,0.857601076645,0.907443973174,0.873188309741,0.917035558936,0.886780956992,0.92550684748,0.898696615603,0.933013712405,0.90918784234,0.939684956024,0.9184589649,0.945628233538,0.926677668663,0.950934391703,0.933983222896,0.955680718969,0.940492471718,0.9599334282,0.946304294498,0.963749585636,0.951502991257,0.967178632062,0.956160895954,0.970263598173,0.960340424066,0.973042086923,0.964095698302,0.975547075737,0.967473854645,0.977807577642,0.970516102695,0.979849190627,0.973258594688,0.98169455749,0.975733143777,0.98336375333,0.977967822289,0.984874614022,0.979987463458,0.98624301618,0.981814084853,0.987483116952,0.983467247761,0.988607560325,0.984964363796,0.989627655353,0.986320957703,0.990553530695,0.98755089362,0.991394269076,0.988666570609};
	for (int i = 0;i<70;i++){
		if ((1-delta)<=probs[i]){
			return (i+1);
		}
	}
	assert(false);
    cout << "ERROR" << endl;
	exit(-1);
}

#endif
